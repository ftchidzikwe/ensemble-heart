
    /*<![CDATA[*/
    $(function () {
        Highcharts.setOptions({
            lang: {
                decimalPoint: '.',
                thousandsSep: ','
            }
        });

        drawNeuralNetworkErrorGraph();
    });



function drawNeuralNetworkErrorGraph() {
    var neuralNetworkErrorChart = Highcharts.chart('neuralnetworkgraph', {

        tooltip: {
            crosshairs: true
        },
        chart: {
            type: 'line',
            margin: 75,
            options3d: {
                enabled: true,
                alpha: 15,
                beta: 15,
                depth: 110
            }
        },
        title: {
            text: 'Plot of Errors'
        },
        xAxis: {
            categories: /*[[${iterations}]]*/ []
        },
        yAxis: {
            title: {
                text: 'Error'
            }
        },
        tooltip: {
            crosshairs: [true,true]
        },
        plotOptions: {
            column: {
                depth: 60,
                stacking: true,
                grouping: false,
                groupZPadding: 10
            }
        },
        series: [{
            name: 'NeuralNetwork',
            data: /*[[${errors}]]*/ []
        }]
    });
}


