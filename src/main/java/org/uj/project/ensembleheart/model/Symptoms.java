package org.uj.project.ensembleheart.model;

import lombok.Data;

import javax.persistence.*;

/**
 * @author fchidzikwe
 */

@Entity

@Data
public class Symptoms {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne
    private User user;

    private double age,sex,cp,trestbps,chol,fbs,restecg,thalach,exang,oldpeak,slope,ca,thal;


}
