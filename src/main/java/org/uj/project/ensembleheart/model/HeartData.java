package org.uj.project.ensembleheart.model;

import com.opencsv.bean.CsvBindByName;
import lombok.Data;
import org.csveed.annotations.CsvFile;

/**
 * @author fchidzikwe
 */

@Data
@CsvFile(comment = '%', quote='\'', escape='\\', separator=',')
public class HeartData {
    @CsvBindByName
    double age;

    @CsvBindByName
    double sex;

    @CsvBindByName
    double cp;

    @CsvBindByName
    double trestbps;

    @CsvBindByName
    double chol;

    @CsvBindByName
    double fbs;

    @CsvBindByName
    double restecg;

    @CsvBindByName
    double thalach;

    @CsvBindByName
    double exang;

    @CsvBindByName
    double oldpeak;

    @CsvBindByName
    double slope;

    @CsvBindByName
    double ca;

    @CsvBindByName
    double thal;

    @CsvBindByName
    double target;


    public double[] getInputArray() throws IllegalAccessException {

        double[] input = new double[13];
        for (int i = 0; i < input.length; i++) {
            input[i] = (double) this.getClass().getDeclaredFields()[i].get(this);
        }
        return input;
    }


    public double[] getOutputArray() throws IllegalAccessException {
        double[] output = new double[1];
        int outputStartCsvIndex = 13;
        for (int i = 0; i < output.length; i++) {
            output[i] = (double) this.getClass().getDeclaredFields()[outputStartCsvIndex].get(this);
            outputStartCsvIndex++;
        }
        return output;
    }

    public double[] getRowArray() throws IllegalAccessException {

        double[] row = new double[14];
        for (int i = 0; i < row.length; i++) {
            row[i] = (double) this.getClass().getDeclaredFields()[i].get(this);
        }
        return row;
    }


    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();


        sb.append(System.lineSeparator());
        sb.append("HeartData{");
        sb.append("age=").append(age);
        sb.append(", sex=").append(sex);
        sb.append(", cp=").append(cp);
        sb.append(", trestbps=").append(trestbps);
        sb.append(", chol=").append(chol);
        sb.append(", fbs=").append(fbs);
        sb.append(", restecg=").append(restecg);
        sb.append(", thalach=").append(thalach);
        sb.append(", exang=").append(exang);
        sb.append(", oldpeak=").append(oldpeak);
        sb.append(", slope=").append(slope);
        sb.append(", ca=").append(ca);
        sb.append(", thal=").append(thal);
        sb.append(", target=").append(target);
        sb.append('}');
        sb.append(System.lineSeparator());

        return sb.toString();
    }
}
