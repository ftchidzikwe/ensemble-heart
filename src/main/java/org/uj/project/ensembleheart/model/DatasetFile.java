package org.uj.project.ensembleheart.model;

import lombok.Data;

/**
 * @author fchidzikwe
 */

@Data
public class DatasetFile {

    private Long id;


    private DataSet dataSet;

    private Integer missingData;

}
