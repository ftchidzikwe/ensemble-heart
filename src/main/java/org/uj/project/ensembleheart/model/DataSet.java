package org.uj.project.ensembleheart.model;

import lombok.Data;

import javax.persistence.Id;
import java.util.List;

/**
 * @author fchidzikwe
 */

@Data
public class DataSet   {

    private static final long serialVersionUID = 6529685098267757690L;

    @Id
   private  String datasetName;

    List<HeartData> heartDataSet;

    public DataSet(String datasetName, List<HeartData> heartDataSet) {
        this.datasetName = datasetName;
        this.heartDataSet = heartDataSet;
    }


    public DataSet() {

    }
}
