package org.uj.project.ensembleheart.model;

import lombok.Data;

/**
 * @author fchidzikwe
 */

@Data
public class CervicalCancerData {

    //define inputs
   double   hormonalContraceptives, syphilisSTD,  genitalHerpesSTD, hivSTD,  dxCancer,  dxCIN;


    //define outputs
     double schiller, biopsy;



    public double[] getInputArray() throws IllegalAccessException {

        double[] input = new double[6];
        for(int i =0;i< input.length; i++){
            input[i] = (double) this.getClass().getDeclaredFields()[i].get(this);
        }
        return  input;
    }


    public double[] getOutputArray() throws IllegalAccessException {
        double[] output = new double[2];
        int outputStartCsvIndex =6;
        for(int i =0;i< output.length; i++){
            output[i] = (double) this.getClass().getDeclaredFields()[outputStartCsvIndex].get(this);
            outputStartCsvIndex ++;
        }
        return output;
    }

    public double[] getRowArray() throws IllegalAccessException {

        double[] row = new double[8];
        for(int i =0;i< row.length; i++){
            row[i] = (double) this.getClass().getDeclaredFields()[i].get(this);
        }
        return  row;
    }


}

