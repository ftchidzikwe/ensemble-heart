package org.uj.project.ensembleheart.model;

import java.util.Arrays;
import java.util.List;

/**
 * @author fchidzikwe
 */


public enum Missingness {


    FIVE("FIVE"), TEN("TEN"), FIFTEEN("FIFTEEN"), TWENTY("TWENTY"), TWENTY_FIVE("TWENTY_FIVE"), THIRTY("THIRTY"), THIRTY_FIVE("THIRTY_FIVE"), FORTY("FORTY"),
    FORTY_FIVE("FORTY_FIVE"), FIFTY("FIFTY"), FIFTY_FIVE("FIFTY_FIVE"), SIXTY("SIXTY"), SIXTY_FIVE("SIXTY_FIVE"), SEVENTY("SEVENTY"), SEVENTY_FIVE("SEVENTY_FIVE"), EIGHTY("EIGHTY");


    private final String name;

    Missingness(String name) {
        this.name = name;
    }

    public static List<Missingness> getMissingValuesAsList() {

        return Arrays.asList(
                FIVE, TEN, FIFTEEN, TWENTY, TWENTY_FIVE, THIRTY, THIRTY_FIVE, FORTY,
                FORTY_FIVE, FIFTY, FIFTY_FIVE, SIXTY, SIXTY_FIVE, SEVENTY, SEVENTY_FIVE, EIGHTY);
    }

}
