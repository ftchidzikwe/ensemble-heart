package org.uj.project.ensembleheart.model;

import lombok.Data;

import javax.persistence.*;

/**
 * @author fchidzikwe
 */

@Data
@Entity
public class DataManipulation {

    @Enumerated(EnumType.STRING)
    Missingness percentageMissing;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("{");
        sb.append("id= ").append(id);
        sb.append("|| percentageMissing=").append(percentageMissing);
        sb.append('}');
        return sb.toString();
    }
}
