package org.uj.project.ensembleheart.model;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

/**
 * @author fchidzikwe
 */

@Entity
@Data
public class Prediction {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private  Long id;

    @Column(name = "prediction_date")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private Date predictionDate;

    @Column(name = "result")
    private  String result;


    @Column(name = "color")
    private  String color;

    @Column(name = "confidence")
    private  double confidence;

    @OneToOne
    private User user;


    @Transient
    public boolean isNew() {
        return (getId() == null);
    }
}
