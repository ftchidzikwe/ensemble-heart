package org.uj.project.ensembleheart.model;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.util.Date;
import java.util.Set;

/**
 * @author fchidzikwe
 */

@Data
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "email", nullable = false, unique = true)
    @Email(message = "Invalid e-mail")
    @NotEmpty(message = "e-mail cannot be empty")
    private String email;


    @Column(name = "password")
    @NotEmpty(message = "Cannot be empty")
    private String password;


    @Column(name = "dob")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private Date dob;

    @Transient
    private String age;

    @Column(name = "user_name")
    @NotEmpty(message = "username cannot be empty")
    private String username;

    @Column(name = "active")
    private int active;


    @ManyToMany
    @JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;

    public User(long id, int active, String email, String lastName, String name, String phoneNumber, String password, String toString) {
    }

    public User() {
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("User{");
        sb.append("id=").append(id);
        sb.append(", email='").append(email).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append(", dob=").append(dob);
        sb.append(", username='").append(username).append('\'');
        sb.append(", active=").append(active);
        sb.append(", roles=").append(roles);
        sb.append('}');
        return sb.toString();
    }
}
