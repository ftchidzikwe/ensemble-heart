package org.uj.project.ensembleheart.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.uj.project.ensembleheart.model.DataManipulation;
import org.uj.project.ensembleheart.repository.DataManipulationRepository;
import org.uj.project.ensembleheart.service.DataManipulationService;

import java.util.List;
import java.util.Optional;

/**
 * @author fchidzikwe
 */

@Service
public class DataManipulationServiceImpl implements DataManipulationService {

    private DataManipulationRepository dataManipulationRepository;


    @Autowired
    public DataManipulationServiceImpl(DataManipulationRepository dataManipulationRepository) {
        this.dataManipulationRepository = dataManipulationRepository;
    }

    @Override
    public DataManipulation save(DataManipulation dataManipulation) {
        return dataManipulationRepository.save(dataManipulation);
    }

    @Override
    public Optional<DataManipulation> findById(Long id) {
        return dataManipulationRepository.findById(id);
    }

    @Override
    public void deleteDatamanipulation(DataManipulation dataManipulation) {
        dataManipulationRepository.delete(dataManipulation);
    }

    @Override
    public List<DataManipulation> findAll() {
        return dataManipulationRepository.findAll();
    }
}
