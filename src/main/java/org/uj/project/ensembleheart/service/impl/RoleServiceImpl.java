package org.uj.project.ensembleheart.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.uj.project.ensembleheart.model.Role;
import org.uj.project.ensembleheart.repository.RoleRepository;
import org.uj.project.ensembleheart.service.RoleService;

/**
 * @author fchidzikwe
 */

@Service
public class RoleServiceImpl implements RoleService {

    private RoleRepository roleRepository;

    @Autowired
    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public Role findById(Long id) {
        return roleRepository.getOne(id);
    }

    @Override
    public Role findByRoleName(String roleName) {
        return roleRepository.findRoleByName(roleName);
    }
}
