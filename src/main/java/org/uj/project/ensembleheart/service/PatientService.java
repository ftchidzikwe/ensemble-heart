package org.uj.project.ensembleheart.service;



import org.uj.project.ensembleheart.model.Patient;

import java.util.List;
import java.util.Optional;

/**
 * @author fchidzikwe
 */
public interface PatientService {


    Patient save(Patient patient);

    Optional<Patient> findByPatientId(Long id);

    void deletePatient(Patient patient);

    List<Patient> findaAll();

    List<Patient> findPatients(String seractTerm);

}
