package org.uj.project.ensembleheart.service;

import org.uj.project.ensembleheart.model.Symptoms;
import org.uj.project.ensembleheart.model.User;

/**
 * @author fchidzikwe
 */
public interface SymptomsService {

    void delete(Symptoms symptoms);

    void save(Symptoms symptoms);


    Symptoms findByUser(User patient);

}
