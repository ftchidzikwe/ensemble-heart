package org.uj.project.ensembleheart.service;

import org.uj.project.ensembleheart.model.DataManipulation;

import java.util.List;
import java.util.Optional;

/**
 * @author fchidzikwe
 */
public interface DataManipulationService {

    DataManipulation save(DataManipulation dataManipulation);

    Optional<DataManipulation> findById(Long id);

    void deleteDatamanipulation(DataManipulation dataManipulation);

    List<DataManipulation> findAll();

}
