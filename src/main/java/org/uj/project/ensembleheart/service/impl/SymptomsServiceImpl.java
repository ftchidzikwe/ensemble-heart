package org.uj.project.ensembleheart.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.uj.project.ensembleheart.model.Symptoms;
import org.uj.project.ensembleheart.model.User;
import org.uj.project.ensembleheart.repository.SymptomsRepository;
import org.uj.project.ensembleheart.service.SymptomsService;

/**
 * @author fchidzikwe
 */

@Service
public class SymptomsServiceImpl implements SymptomsService {

    private SymptomsRepository symptomsRepository;


    @Autowired
    public SymptomsServiceImpl(SymptomsRepository symptomsRepository) {
        this.symptomsRepository = symptomsRepository;
    }

    @Override
    public void delete(Symptoms symptoms) {
        symptomsRepository.delete(symptoms);

    }

    @Override
    public void save(Symptoms symptoms) {
        symptomsRepository.save(symptoms);

    }

    @Override
    public Symptoms findByUser(User patient) {
        return symptomsRepository.findByUser(patient);
    }
}
