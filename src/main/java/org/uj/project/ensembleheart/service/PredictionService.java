package org.uj.project.ensembleheart.service;

import org.uj.project.ensembleheart.model.Prediction;
import org.uj.project.ensembleheart.model.User;

import java.util.List;

/**
 * @author fchidzikwe
 */
public interface PredictionService {

    void save(Prediction diagnosis);

    List<Prediction> findByUser(User patient);
}
