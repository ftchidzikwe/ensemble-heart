package org.uj.project.ensembleheart.service;



import org.uj.project.ensembleheart.model.User;

import java.util.List;

/**
 * @author fchidzikwe
 */
public interface UserService {


     User findByEmail(String email);

     User getLoggedInUser();

     User findByUsername(String userName);


     User findById(Long id);

     void deleteUser(Long id);

     void saveUser(User user);

     List<User> findAll();


}
