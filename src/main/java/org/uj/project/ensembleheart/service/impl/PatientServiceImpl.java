package org.uj.project.ensembleheart.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.uj.project.ensembleheart.model.Patient;
import org.uj.project.ensembleheart.repository.PatientRepository;
import org.uj.project.ensembleheart.service.PatientService;

import java.util.List;
import java.util.Optional;

/**
 * @author fchidzikwe
 */

@Service
public class PatientServiceImpl  implements PatientService {


    private PatientRepository patientRepository;

    @Autowired
    public PatientServiceImpl(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }



    @Override
    public Patient save(Patient patient) {
        return patientRepository.save(patient);
    }

    @Override
    public Optional<Patient> findByPatientId(Long id) {
        return patientRepository.findById(id);
    }

    @Override
    public void deletePatient(Patient patient) {
         patientRepository.delete(patient);
    }

    @Override
    public List<Patient> findaAll() {
        return patientRepository.findAll();
    }

    @Override
    public List<Patient> findPatients(String seractTerm) {
        return patientRepository.findPatientByLastnameOrFirstnameOrPatientNumber(seractTerm);
    }

}
