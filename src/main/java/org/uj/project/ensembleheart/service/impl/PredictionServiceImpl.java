package org.uj.project.ensembleheart.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.uj.project.ensembleheart.model.Prediction;
import org.uj.project.ensembleheart.model.User;
import org.uj.project.ensembleheart.repository.PredictionRepository;
import org.uj.project.ensembleheart.service.PredictionService;

import java.util.List;

/**
 * @author fchidzikwe
 */

@Service
public class PredictionServiceImpl implements PredictionService {

    private PredictionRepository  predictionRepository;

    @Autowired
    public PredictionServiceImpl(PredictionRepository predictionRepository) {
        this.predictionRepository = predictionRepository;
    }

    @Override
    public void save(Prediction prediction) {

        predictionRepository.save(prediction);

    }

    @Override
    public List<Prediction> findByUser(User patient) {
        return predictionRepository.findByUser(patient);
    }
}
