package org.uj.project.ensembleheart.service;


import org.uj.project.ensembleheart.model.Role;

/**
 * @author fchidzikwe
 */
public interface RoleService {

    Role findById(Long id);

    Role findByRoleName(String roleName);
}
