package org.uj.project.ensembleheart.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.uj.project.ensembleheart.model.User;
import org.uj.project.ensembleheart.repository.UserRepository;
import org.uj.project.ensembleheart.service.UserService;

import java.util.List;

/**
 * @author fchidzikwe
 */

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;


    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public User findByUsername(String userName) {
        return userRepository.findByUsername(userName);
    }

    public User findById(Long id){
        return  userRepository.getOne(id);
    }

    public void deleteUser(Long id){
        User user = findById(id);
        userRepository.delete(user);
    }

    public void saveUser(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setActive(1);
        userRepository.save(user);
    }

    public List<User> findAll(){

        return   userRepository.findAll();
    }

    public User getLoggedInUser(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        User user = findByUsername(authentication.getName());
        return user;
    }
}
