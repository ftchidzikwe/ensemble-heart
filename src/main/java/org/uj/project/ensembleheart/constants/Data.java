package org.uj.project.ensembleheart.constants;




/**
 * @author fchidzikwe
 */

@lombok.Data
public class Data {

    public static  final String CSV_LOCATION = "data";

    public static String NUERAL_NET_NDIR = "src/main/resources/";



    public static final String CSV_NAME = "heart.csv";



    public static final String NORMALISED_CSV_NAME = "normHeart.csv";

    public static final String TRAINING_DATA_NAME = "trainingHeart.csv";

    public static final String TESTING_DATA_NAME = "testingHeart.csv";

    public static final String NORM_TRAINING_DATA_NAME = "normTrainingHeart.csv";

    public static final String NORM_TESTING_DATA_NAME = "normTestingHeart.csv";

    public  static int AGE=0,SEX=1,CP=2,TRESTBPS=3,CHOL=4,FBS=5,RESTECG=6,THALACH=7,EXANG=8,OLDPEAK=9,SLOPE=10,CA=11,THAL=12,TARGET=13;


    public static String pathToSaveNnOnFinish() {

        return NUERAL_NET_NDIR + "/" + "trainedNeuralNetwork";
    }


    public static String pathToTrainedNN() {

        return NUERAL_NET_NDIR + "/" + "trainedNet.ser";
    }

    public static String pathToTestedNN() {

        return NUERAL_NET_NDIR + "/" + "testedNet.ser";
    }

    public static String getPathToDatasetStorage() {

        return NUERAL_NET_NDIR + "/" ;
    }

    public static String getDataFileLocation() {
        return CSV_LOCATION + "/"+ CSV_NAME;
    }

    public static String getNormalisedDataFileLocation() {
        return CSV_LOCATION + "/" + NORMALISED_CSV_NAME;
    }

    public static String getTrainingDataFileLocation() {
        return CSV_LOCATION + "/" + TRAINING_DATA_NAME;
    }

    public static String getTestingDataFileLocation() {
        return CSV_LOCATION + "/" + TESTING_DATA_NAME;
    }

    public static String getNormalisedTrainingDataFileLocation() {
        return CSV_LOCATION + "/" + NORM_TRAINING_DATA_NAME;
    }

    public static String getNormalisedTestingDataFileLocation() {
        return CSV_LOCATION + "/" + NORM_TESTING_DATA_NAME;
    }


}
