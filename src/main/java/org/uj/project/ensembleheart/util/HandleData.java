package org.uj.project.ensembleheart.util;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.uj.project.ensembleheart.constants.Data;
import org.uj.project.ensembleheart.model.HeartData;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;



/**
 * @author fchidzikwe
 */

@lombok.Data
@Component
public class HandleData {

    private Logger  logger = LoggerFactory.getLogger(HandleData.class);


    int numberOfIntsances = 0;

    List<HeartData> heartDataList = new ArrayList<>();
    List<HeartData> trainingDataList = new ArrayList<>();
    List<HeartData> testingDataList = new ArrayList<>();



    public List<HeartData> csvToJavaObjects(String dataPath) {

        String csvFile = Data.getNormalisedDataFileLocation();
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";

        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] values = line.split(cvsSplitBy);

                HeartData heartData = new HeartData();


                        /**   INPUTS
                         * age, numOfSexualPartners,  firstSexualIntercouseAge,  numOfPregnances,  smokes,  yearsOfSmoking,  smokePacksPerYear,  hormonalContraceptives,
                        yrsOfTakingHormonalContraceptives,  IUD,  IDUYears,  STDs,  numOfSTD,  condylomatosisSTD,  cervicalCondylomatosisSTD,  vaginalCondylomatosisSTD,
                        vulvoPerinealCondylomatosisSTD,  syphilisSTD,  pelvicInflammatoryDiseaseSTD,  genitalHerpesSTD,  molluscumContagiosumSTD,  aidsSTD,  hivSTD,  hepatitisBSTD,
                        hpvSTD,  numOfDiagnosisSTD,  timeSinceFirstDiagnosisSTD,  timeSinceLAstDiagnosisSTD,  dxCancer,  dxCIN,  dxHPV,  Dx;

                         */


                heartDataList.add(heartData);

            }

            numberOfIntsances += heartDataList.size();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return heartDataList;

    }












}
