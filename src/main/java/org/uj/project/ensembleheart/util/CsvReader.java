package org.uj.project.ensembleheart.util;

import org.csveed.api.CsvClient;
import org.csveed.api.CsvClientImpl;
import org.springframework.stereotype.Component;
import org.uj.project.ensembleheart.constants.Data;
import org.uj.project.ensembleheart.model.DataSet;
import org.uj.project.ensembleheart.model.HeartData;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author fchidzikwe
 */



@Component
@lombok.Data
public class CsvReader {

    Map<String, Integer>  labelMissingCount = new HashMap<>();


    List<HeartData> heartDataList = new ArrayList<>();

    /**
     * readCsv
     * This methods read from csv to  Dataset
     */
   public DataSet  readCsv(String path) throws IOException {

       DataSet dataSet = new DataSet();
       BufferedReader br = null;
       String line = "";
       String cvsSplitBy = ",";


       try {

           br = new BufferedReader(new FileReader(path));

           br.readLine();

           int i =0 ;
           while ((line = br.readLine()) != null) {
               //
               i++;
               // use comma as separator
               String[] values = line.split(cvsSplitBy);
               HeartData heartData=  makeheartDataObject(values);

               heartDataList.add(heartData);
           }

       }catch (Exception e){

           e.printStackTrace();
       }

       //shuffle the dataset
     //  Collections.shuffle(heartDataList);

       dataSet.setHeartDataSet(heartDataList);

       return  dataSet;

   }


   public DataSet readFile(String pathName) throws FileNotFoundException {
       Reader reader  = new BufferedReader(new FileReader(pathName));
       CsvClient<HeartData> csvReader = new CsvClientImpl<HeartData>(reader, HeartData.class);
       final List<HeartData> heartDataList = csvReader.readBeans();


       DataSet dataSet = new DataSet();
       dataSet.setHeartDataSet(heartDataList);
      return  dataSet;
   }





    //csv contains string values



    public HeartData makeheartDataObject(String[] rowValues) {
        HeartData heartData = new HeartData();
        for (int i = 0; i < rowValues.length; i++) {


            String age = rowValues[Data.AGE].trim();
            heartData.setAge(Double.parseDouble(age));
            heartData.setSex(Double.parseDouble(rowValues[Data.SEX]));
            heartData.setCp(Double.parseDouble(rowValues[Data.CP]));
            heartData.setTrestbps(Double.parseDouble(rowValues[Data.TRESTBPS]));
            heartData.setChol(Double.parseDouble(rowValues[Data.CHOL]));
            heartData.setFbs(Double.parseDouble(rowValues[Data.FBS]));
            heartData.setRestecg(Double.parseDouble(rowValues[Data.RESTECG]));
            heartData.setThalach(Double.parseDouble(rowValues[Data.THALACH]));
            heartData.setExang(Double.parseDouble(rowValues[Data.EXANG]));
            heartData.setOldpeak(Double.parseDouble(rowValues[Data.OLDPEAK]));
            heartData.setSlope(Double.parseDouble(rowValues[Data.SLOPE]));
            heartData.setCa(Double.parseDouble(rowValues[Data.CA]));
            heartData.setThal(Double.parseDouble(rowValues[Data.THAL]));


            heartData.setTarget(Double.parseDouble(rowValues[Data.TARGET]));

        }
        return heartData;
    }



}
