package org.uj.project.ensembleheart.util;

import org.springframework.stereotype.Component;
import org.uj.project.ensembleheart.constants.Data;
import org.uj.project.ensembleheart.model.DataSet;
import org.uj.project.ensembleheart.model.HeartData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author fchidzikwe
 */

@Component
public class DataNormalisation {

    public DataSet normaliseData(DataSet dataSet) throws IllegalAccessException {

        DataSet normalisedDataset =new DataSet();
        List<HeartData> heartDataList = dataSet.getHeartDataSet();
        List<HeartData> normalisedheartDataList = new ArrayList<>();




        //variables for each attribute
        //define inputs


        // 'Hormonal Contraceptives (years)',STDs:syphilis,'STDs:genital herpes',STDs:HIV,Dx:Cancer,Dx:CIN,Schiller,Biopsy
        List<Double> age= new ArrayList<>(), sex= new ArrayList<>(), cp= new ArrayList<>(), trestbps= new ArrayList<>(),
                chol= new ArrayList<>(),fbs= new ArrayList<>(), restecg= new ArrayList<>(), thalach= new ArrayList<>(),
                exang= new ArrayList<>(), oldpeak= new ArrayList<>(), slope= new ArrayList<>(), ca= new ArrayList<>(),
                thal= new ArrayList<>();

        //define outputs
        List<Double>  target= new ArrayList<>();

        //get List of Cerical Cancer
        //get values for each attribute
        for(HeartData heartData: heartDataList){
           double[] rowValues =  heartData.getRowArray();

            age.add(rowValues[Data.AGE]);
            sex.add(rowValues[Data.SEX]);
            cp.add(rowValues[Data.CP]);
            trestbps.add(rowValues[Data.TRESTBPS]);
            chol.add(rowValues[Data.CHOL]);
            fbs.add(rowValues[Data.FBS]);
            restecg.add(rowValues[Data.RESTECG]);
            thalach.add(rowValues[Data.THALACH]);
            exang.add(rowValues[Data.EXANG]);
            oldpeak.add(rowValues[Data.OLDPEAK]);
            slope.add(rowValues[Data.SLOPE]);
            ca.add(rowValues[Data.CA]);

            thal.add(rowValues[Data.THAL]);
            //OUTPUT

            target.add(rowValues[Data.TARGET]);

        }


        //normalise for each object, for each attribute we need 2 variables, min and max

        for(HeartData heartData: heartDataList){
            Map<String , List<Double>> attributeValuesMap = new HashMap<>(14);

            attributeValuesMap.put("age", age);
            attributeValuesMap.put("sex", sex);
            attributeValuesMap.put("cp", cp);
            attributeValuesMap.put("trestbps", trestbps);
            attributeValuesMap.put("chol", chol);
            attributeValuesMap.put("fbs", fbs);

            attributeValuesMap.put("restecg", restecg);
            attributeValuesMap.put("thalach", thalach);
            attributeValuesMap.put("exang", exang);

            // output
            attributeValuesMap.put("oldpeak", oldpeak);

            attributeValuesMap.put("slope", slope);
            attributeValuesMap.put("ca", ca);
            attributeValuesMap.put("thal", thal);

            attributeValuesMap.put("target", target);


            normaliseObject(heartData, attributeValuesMap);


            normalisedheartDataList.add(heartData);



        }

        normalisedDataset.setHeartDataSet(normalisedheartDataList);
        normalisedDataset.setDatasetName("normalisedFullDataset");

        return  normalisedDataset;
    }

    private void normaliseObject(HeartData heartData, Map<String, List<Double>> attributeValuesMap) {



        normaliseAge(heartData, attributeValuesMap.get("age"));
        normaliseSex(heartData, attributeValuesMap.get("sex"));
        normaliseCp(heartData, attributeValuesMap.get("cp"));
        normaliseTrestbps(heartData, attributeValuesMap.get("trestbps"));
        normaliseChol(heartData, attributeValuesMap.get("chol"));
        normaliseFbs(heartData, attributeValuesMap.get("fbs"));
        normaliseRestecg(heartData, attributeValuesMap.get("restecg"));
        normaliseThalach(heartData, attributeValuesMap.get("thalach"));
        normaliseExang(heartData, attributeValuesMap.get("exang"));
        normaliseOldpeakList(heartData, attributeValuesMap.get("oldpeak"));
        normaliseSlope(heartData, attributeValuesMap.get("slope"));
        normaliseCa(heartData, attributeValuesMap.get("ca"));
        normaliseThal(heartData, attributeValuesMap.get("thal"));
        normaliseTarget(heartData, attributeValuesMap.get("target"));

    }

    private void normaliseTarget(HeartData heartData, List<Double> targetList) {
        double min = targetList.stream().mapToDouble(value -> value).min().getAsDouble();
        double max = targetList.stream().mapToDouble(value -> value).max().getAsDouble();
        double value = heartData.getTarget();
        double norm = (value - min)/(max - min);
        heartData.setTarget(norm);
    }

    private void normaliseThal(HeartData heartData, List<Double> thalList) {
        double min = thalList.stream().mapToDouble(value -> value).min().getAsDouble();
        double max = thalList.stream().mapToDouble(value -> value).max().getAsDouble();
        double value = heartData.getThal();
        double norm = (value - min)/(max - min);
        heartData.setThal(norm);
    }

    private void normaliseCa(HeartData heartData, List<Double> caList) {
        double min = caList.stream().mapToDouble(value -> value).min().getAsDouble();
        double max = caList.stream().mapToDouble(value -> value).max().getAsDouble();
        double value = heartData.getCa();
        double norm = (value - min)/(max - min);
        heartData.setCa(norm);
    }

    private void normaliseSlope(HeartData heartData, List<Double> slopeList) {
        double min = slopeList.stream().mapToDouble(value -> value).min().getAsDouble();
        double max = slopeList.stream().mapToDouble(value -> value).max().getAsDouble();
        double value = heartData.getSlope();
        double norm = (value - min)/(max - min);
        heartData.setSlope(norm);
    }

    private void normaliseOldpeakList(HeartData heartData, List<Double> oldpeakList) {
        double min = oldpeakList.stream().mapToDouble(value -> value).min().getAsDouble();
        double max = oldpeakList.stream().mapToDouble(value -> value).max().getAsDouble();
        double value = heartData.getOldpeak();
        double norm = (value - min)/(max - min);
        heartData.setOldpeak(norm);
    }

    private void normaliseExang(HeartData heartData, List<Double> exangList) {
        double min = exangList.stream().mapToDouble(value -> value).min().getAsDouble();
        double max = exangList.stream().mapToDouble(value -> value).max().getAsDouble();
        double value = heartData.getExang();
        double norm = (value - min)/(max - min);
        heartData.setExang(norm);
    }

    private void normaliseThalach(HeartData heartData, List<Double> thalachList) {
        double min = thalachList.stream().mapToDouble(value -> value).min().getAsDouble();
        double max = thalachList.stream().mapToDouble(value -> value).max().getAsDouble();
        double value = heartData.getThalach();
        double norm = (value - min)/(max - min);
        heartData.setThalach(norm);
    }



    private void normaliseFbs(HeartData heartData, List<Double> fbsList) {
        double min = fbsList.stream().mapToDouble(value -> value).min().getAsDouble();
        double max = fbsList.stream().mapToDouble(value -> value).max().getAsDouble();
        double value = heartData.getFbs();
        double norm = (value - min)/(max - min);
        heartData.setFbs(norm);
    }

    private void normaliseAge(HeartData heartData, List<Double> ageList) {
        double min = ageList.stream().mapToDouble(value -> value).min().getAsDouble();
        double max = ageList.stream().mapToDouble(value -> value).max().getAsDouble();
        double value = heartData.getAge();
        double norm = (value - min)/(max - min);
        heartData.setAge(norm);
    }

    private void normaliseChol(HeartData heartData, List<Double> chols) {
        double min = chols.stream().mapToDouble(value -> value).min().getAsDouble();
        double max = chols.stream().mapToDouble(value -> value).max().getAsDouble();
        double value = heartData.getChol();
        double norm = (value - min)/(max - min);
        heartData.setChol(norm);
    }





    private void normaliseTrestbps(HeartData heartData, List<Double> trestbpsList) {
        double min = trestbpsList.stream().mapToDouble(value -> value).min().getAsDouble();
        double max = trestbpsList.stream().mapToDouble(value -> value).max().getAsDouble();
        double value = heartData.getTrestbps();
        double norm = (value - min)/(max - min);
        heartData.setTrestbps(norm);
    }



    private void normaliseCp(HeartData heartData, List<Double> cps) {
        double min = cps.stream().mapToDouble(value -> value).min().getAsDouble();
        double max = cps.stream().mapToDouble(value -> value).max().getAsDouble();
        double value = heartData.getCp();
        double norm = (value - min)/(max - min);
        heartData.setCp(norm);
    }



    private void normaliseSex(HeartData heartData, List<Double> sexs) {
        double min = sexs.stream().mapToDouble(value -> value).min().getAsDouble();
        double max = sexs.stream().mapToDouble(value -> value).max().getAsDouble();
        double value = heartData.getSex();
        double norm = (value - min)/(max - min);
        heartData.setSex(norm);
    }


    private void normaliseRestecg(HeartData heartData, List<Double> restecgList) {
        double min = restecgList.stream().mapToDouble(value -> value).min().getAsDouble();
        double max = restecgList.stream().mapToDouble(value -> value).max().getAsDouble();
        double value = heartData.getRestecg();
        double norm = (value - min)/(max - min);
        heartData.setRestecg(norm);
    }



}
