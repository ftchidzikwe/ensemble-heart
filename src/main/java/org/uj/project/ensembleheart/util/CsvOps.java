package org.uj.project.ensembleheart.util;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.springframework.stereotype.Component;
import org.uj.project.ensembleheart.constants.Data;
import org.uj.project.ensembleheart.model.DataSet;
import org.uj.project.ensembleheart.model.HeartData;

import java.io.FileReader;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * @author fchidzikwe
 */

@Component
public class CsvOps {

    public DataSet readCsvAndParseToObject(String csvPath) throws IOException {


        List<HeartData> heartDataList = new ArrayList<>();

        CSVReader reader = new CSVReader(new FileReader(csvPath), ',', '"', 1);

        //Read all rows at once
        List<String[]> allRows = reader.readAll();

        //Read CSV line by line and use the string array as you want
        for(String[] row : allRows){
            HeartData data = makeheartDataObject(row);

            heartDataList.add(data);
        }
        DataSet dataSet = new DataSet();
        dataSet.setHeartDataSet(heartDataList);

        return  dataSet;
    }

    public void writeToCsv(DataSet dataSet, String writeToPath)throws IOException,
            CsvDataTypeMismatchException,
            CsvRequiredFieldEmptyException {

            try (
                    Writer writer = Files.newBufferedWriter(Paths.get(writeToPath));
            ) {
                StatefulBeanToCsv<HeartData> beanToCsv = new StatefulBeanToCsvBuilder(writer)
                        .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                        .build();


                System.out.println("=======before writing to csv "+ dataSet.getHeartDataSet());

                beanToCsv.write(dataSet.getHeartDataSet());
            }
    }


    public HeartData makeheartDataObject(String[] rowValues) {
        HeartData heartData = new HeartData();
        for (int i = 0; i < rowValues.length; i++) {


            String age = rowValues[Data.AGE].trim();
            heartData.setAge(Double.parseDouble(age));
            heartData.setSex(Double.parseDouble(rowValues[Data.SEX]));
            heartData.setCp(Double.parseDouble(rowValues[Data.CP]));
            heartData.setTrestbps(Double.parseDouble(rowValues[Data.TRESTBPS]));
            heartData.setChol(Double.parseDouble(rowValues[Data.CHOL]));
            heartData.setFbs(Double.parseDouble(rowValues[Data.FBS]));
            heartData.setRestecg(Double.parseDouble(rowValues[Data.RESTECG]));
            heartData.setThalach(Double.parseDouble(rowValues[Data.THALACH]));
            heartData.setExang(Double.parseDouble(rowValues[Data.EXANG]));
            heartData.setOldpeak(Double.parseDouble(rowValues[Data.OLDPEAK]));
            heartData.setSlope(Double.parseDouble(rowValues[Data.SLOPE]));
            heartData.setCa(Double.parseDouble(rowValues[Data.CA]));
            heartData.setThal(Double.parseDouble(rowValues[Data.THAL]));


            heartData.setTarget(Double.parseDouble(rowValues[Data.TARGET]));

        }
        return heartData;
    }

}
