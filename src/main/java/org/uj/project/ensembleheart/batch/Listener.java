package org.uj.project.ensembleheart.batch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.uj.project.ensembleheart.model.User;
import org.uj.project.ensembleheart.repository.UserRepository;

import java.util.List;

public class Listener extends JobExecutionListenerSupport {
    private static final Logger log = LoggerFactory.getLogger(Listener.class);
 
    @Autowired
    private UserRepository userRepository;
 
    public Listener(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @Override
    public void afterJob(JobExecution jobExecution) {
        if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
            log.info("Finish Job! Check the results");
 
            List<User> users = userRepository.findAll();
 
            for (User user : users) {
                log.info("Found <" + user + "> in the database.");
            }
        }
    }
}