package org.uj.project.ensembleheart.batch;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.uj.project.ensembleheart.model.Role;
import org.uj.project.ensembleheart.model.User;

import java.util.Set;


public class Processor implements ItemProcessor<User, User> {
 
    private static final Logger log = LoggerFactory.getLogger(Processor.class);
 
    @Override
    public User process(User user) throws Exception {
       
         
        final int active = user.getActive();
        final long id = user.getId();
        final String email = user.getEmail();
        final String lastName = user.getUsername();
        final String name = user.getUsername();
        final String phoneNumber = user.getUsername();
        final String password = user.getPassword();
        final Set<Role> roles =user.getRoles();

        final User fixedUser = new  User(id,active , email,lastName, name,phoneNumber, password,roles.toString());
 
        log.info("Converting (" + user + ") into (" + fixedUser + ")");
 
        return fixedUser;
    }
}
