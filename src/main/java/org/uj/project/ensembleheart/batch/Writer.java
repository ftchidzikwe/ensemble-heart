package org.uj.project.ensembleheart.batch;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.uj.project.ensembleheart.model.User;
import org.uj.project.ensembleheart.repository.UserRepository;

import java.util.List;


public class Writer implements ItemWriter<User> {
	
 
	@Autowired
    private final UserRepository userRepository;
     
    public Writer(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
 
    @Override
    public void write(List<? extends User> users) throws Exception {
        userRepository.saveAll(users);
    }
}