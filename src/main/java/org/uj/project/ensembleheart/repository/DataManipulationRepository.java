package org.uj.project.ensembleheart.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.uj.project.ensembleheart.model.DataManipulation;


/**
 * @author fchidzikwe
 */


public interface DataManipulationRepository extends JpaRepository<DataManipulation, Long> {
}
