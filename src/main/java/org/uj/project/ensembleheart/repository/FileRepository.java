package org.uj.project.ensembleheart.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.uj.project.ensembleheart.model.File;


/**
 * @author fchidzikwe
 */
public interface FileRepository extends JpaRepository<File, Long> {
    File findFirstByOrderByIdDesc();

    File findByFileName(String name);
}
