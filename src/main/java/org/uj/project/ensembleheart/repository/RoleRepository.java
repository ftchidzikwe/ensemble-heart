package org.uj.project.ensembleheart.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.uj.project.ensembleheart.model.Role;

/**
 * @author fchidzikwe
 */
public interface RoleRepository extends JpaRepository<Role, Long> {
    Role findRoleByName(String name);
}
