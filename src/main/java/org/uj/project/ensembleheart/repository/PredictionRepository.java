package org.uj.project.ensembleheart.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.uj.project.ensembleheart.model.Prediction;
import org.uj.project.ensembleheart.model.User;

import java.util.List;

/**
 * @author fchidzikwe
 */
public interface PredictionRepository extends JpaRepository<Prediction, Long> {

    List<Prediction> findByUser(User patient);
}
