package org.uj.project.ensembleheart.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.uj.project.ensembleheart.model.Patient;

import java.util.List;

/**
 * @author fchidzikwe
 */
public interface PatientRepository extends JpaRepository<Patient, Long> {
    Patient save(Patient patient);

    @Query("select p from Patient p where p.firstName like :searchTerm or p.lastName like :searchTerm")
    List<Patient> findPatientByLastnameOrFirstnameOrPatientNumber(@Param("searchTerm") String searchTerm);

}
