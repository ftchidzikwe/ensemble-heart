package org.uj.project.ensembleheart.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.uj.project.ensembleheart.model.User;

import java.util.List;

/**
 * @author fchidzikwe
 */

@Repository
public interface UserRepository  extends JpaRepository<User, Long>{
    User findByEmail(String email);
    User findByUsername(String username);
    List<User> findAll();
}
