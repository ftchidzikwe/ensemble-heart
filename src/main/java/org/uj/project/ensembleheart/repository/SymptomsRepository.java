package org.uj.project.ensembleheart.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.uj.project.ensembleheart.model.Symptoms;
import org.uj.project.ensembleheart.model.User;

/**
 * @author fchidzikwe
 */
public interface SymptomsRepository extends JpaRepository<Symptoms, Long> {

    Symptoms findByUser(User patient);

}
