package org.uj.project.ensembleheart.neuralnet;

import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.abego.treelayout.TreeForTreeLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.uj.project.ensembleheart.constants.Data;
import org.uj.project.ensembleheart.decisiontree.DecisionTree;
import org.uj.project.ensembleheart.decisiontree.Node;
import org.uj.project.ensembleheart.decisiontree.SampleTreeFactory;
import org.uj.project.ensembleheart.model.DataSet;
import org.uj.project.ensembleheart.model.HeartData;
import org.uj.project.ensembleheart.util.CsvOps;
import org.uj.project.ensembleheart.util.CsvReader;
import org.uj.project.ensembleheart.util.DataNormalisation;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.*;
import java.util.List;

/**
 * @author fchidzikwe
 */
@Component
public class RunNN {

    static CsvOps csvOps = new CsvOps();
    static String TESTINGDATASETNAME = "testing_set";
    static String TRAININGDATASETNAME = "training_set";
    static String DATASETNAME = "full_dataset";
    static String NORMALISEDDATASETNAME = "normalised_full_dataset";
    private static Logger logger = LoggerFactory.getLogger(RunNN.class);
    CsvReader csvReader = new CsvReader();

    public static void main(String[] args) throws IllegalAccessException, IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
        RunNN runNN = new RunNN();
        runNN.processData();

        /**Getting the datasets
         *
         * dataset used for neural network is normalised and dataset used
         * for decision tree is not normalised
         *
         */

        DataSet nnTrainingDataSet = csvOps.readCsvAndParseToObject(Data.getNormalisedTrainingDataFileLocation());
        DataSet nnTestingDataSet = csvOps.readCsvAndParseToObject(Data.getNormalisedTestingDataFileLocation());
        DataSet dtTrainingDataSet = csvOps.readCsvAndParseToObject(Data.getDataFileLocation());
        DataSet dtTestingDataSet = csvOps.readCsvAndParseToObject(Data.getTrainingDataFileLocation());

        /**
         * this is the neural network training
         */

        NeuralNet neuralNet = new NeuralNet(13, 6, 1);
        neuralNet.initialiseWeights();
        neuralNet.startTrainingToError(nnTrainingDataSet, 0.02, 0);
        neuralNet.saveTestedNeuralNetwork(neuralNet);


        /**
         * Decision tree training
         */

        DecisionTree decisionTree = new DecisionTree();

        Node rootNode = new Node(dtTrainingDataSet.getHeartDataSet());
        rootNode.setParent(null);


        // use depth first search
        Stack<Node> tree = new Stack<Node>();
        Stack<Node> printTree = new Stack<Node>();
        printTree.push(rootNode);

        // before passing the node in the training tree check if it is pure

        if (!decisionTree.isPure(rootNode, 0.7)) {
            tree.push(rootNode);
            decisionTree.startTraining( 0.75, tree, printTree);
        }



        NodeView nodeView = new NodeView("", rootNode.getEntropy(), rootNode.getHeartDataList().size());

        // check if node is pure



        System.out.println("====================AFTER DECISION TREE============================");

        System.out.println(printTree);

        System.out.println("====================AFTER DECISION TREE============================");


        System.out.println(System.lineSeparator());

        System.out.println("==================== DECISION TREE TO LIST============================");

        System.out.println(new ArrayList<>(printTree));

        System.out.println("==================== DECISION TREE TO LIST============================");



        // test dt with 5 rows
        double testSize =dtTrainingDataSet.getHeartDataSet().size();

        System.out.println( "------------------test size " + testSize);

        int correctCounts = 0;

        for(int i =0; i< testSize; i++){

            double input [] = dtTrainingDataSet.getHeartDataSet().get(i).getInputArray();

           double desired [] = dtTrainingDataSet.getHeartDataSet().get(i).getOutputArray();


           double output = decisionTree.testDecistionTree(input, printTree);

            System.out.print( " desired ::" + Arrays.toString(desired) + " output : " + output);

           if(output == desired[0]){
               correctCounts ++;
           }



        }


        System.out.println( "**** DECISION TREEE HAS MANAGED TO PREDICTT CORRECTLY " + correctCounts + " out of "+ testSize);


        // classify
        Scanner scanner = new Scanner(System.in);
        double symptoms[] = new double[2];
        for (int i = 0; i < symptoms.length; i++) {
            symptoms[i] = scanner.nextDouble();
        }

        //   NeuralNet classifier = neuralNet.retrieveTestedNN();

        //   double [] results = classifier.forwardPass(symptoms);


    }

    private static void showInDialog(JComponent panel) {
        JDialog dialog = new JDialog();
        Container contentPane = dialog.getContentPane();
        ((JComponent) contentPane).setBorder(BorderFactory.createEmptyBorder(
                10, 10, 10, 10));
        contentPane.add(panel);
        dialog.pack();
        dialog.setLocationRelativeTo(null);
        dialog.setVisible(true);
    }

    private static TreeForTreeLayout<NodeView> getSampleTree(String treeName) {
        TreeForTreeLayout<NodeView> tree;
        if (treeName.equals("2")) {
            tree = SampleTreeFactory.createSampleTree2();
        } else if (treeName.equals("")) {
            tree = SampleTreeFactory.createSampleTree();
        } else {
            throw new RuntimeException(String.format("Invalid tree name: '%s'",
                    treeName));
        }
        return tree;
    }

    private void processData() throws CsvRequiredFieldEmptyException, IOException, CsvDataTypeMismatchException {
        DataSet dataSet = new DataSet();
        try {

            dataSet = csvReader.readFile(Data.getDataFileLocation());
        } catch (IOException e) {
            e.printStackTrace();
        }
        dataSet.setDatasetName(DATASETNAME);

        createTrainingAndTestingDataSet(dataSet);

    }


    /**
     * createTrainingAndTestingDataSet
     * This methods creates the training and testing set 70% -30%
     */

    public void createTrainingAndTestingDataSet(DataSet dataset) throws CsvRequiredFieldEmptyException, IOException, CsvDataTypeMismatchException {

        DataSet trainingSet = new DataSet();
        DataSet testingSet = new DataSet();
        testingSet.setDatasetName(TESTINGDATASETNAME);
        trainingSet.setDatasetName(TRAININGDATASETNAME);

        int sizeOfDataset = dataset.getHeartDataSet().size();
        int trainingMaxIndex = (int) Math.ceil(sizeOfDataset * 0.7);


        List<HeartData> training = dataset.getHeartDataSet().subList(0, trainingMaxIndex);
        List<HeartData> testing = dataset.getHeartDataSet().subList(trainingMaxIndex + 1, sizeOfDataset - 1);

        trainingSet.setHeartDataSet(training);
        trainingSet.setDatasetName(Data.getTrainingDataFileLocation());
        testingSet.setHeartDataSet(testing);
        testingSet.setDatasetName(Data.getTestingDataFileLocation());

        //write to csv

        csvOps.writeToCsv(trainingSet, Data.getTrainingDataFileLocation());
        csvOps.writeToCsv(testingSet, Data.getTestingDataFileLocation());

        // writeCSVFile(trainingSet);
        //  writeCSVFile(testingSet);
        DataNormalisation dataNormalisation = new DataNormalisation();
        try {

            // normalise training set
            DataSet normalisedTrainingSet = dataNormalisation.normaliseData(trainingSet);
            DataSet normalisedTestingSet = dataNormalisation.normaliseData(testingSet);
            normalisedTrainingSet.setDatasetName(Data.getNormalisedTrainingDataFileLocation());
            normalisedTestingSet.setDatasetName(Data.getNormalisedTestingDataFileLocation());

            //write to csv
            csvOps.writeToCsv(normalisedTrainingSet, Data.getNormalisedTrainingDataFileLocation());
            csvOps.writeToCsv(normalisedTestingSet, Data.getNormalisedTestingDataFileLocation());

            //    writeCSVFile(normalisedTrainingSet);
            //  writeCSVFile(normalisedTestingSet);


        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }


    }

}
