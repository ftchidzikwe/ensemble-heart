package org.uj.project.ensembleheart.neuralnet;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author fchidzikwe
 */
@Data
public class HiddenLayer implements Serializable {

    private static final long serialVersionUID = 6529685098267757691L;

    private double bias = 1;

    List<Neuron> neurons = new ArrayList<>();


    HiddenLayer(int numberOfNeurons){
        //bias Neuron



        //input neurons
        for(int i=0;i<numberOfNeurons;i++){
            Neuron neuron = new Neuron();
            neurons.add(neuron);
        }
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("HiddenLayer{");
        sb.append("neurons=").append(neurons);
        sb.append('}');
        return sb.toString();
    }

}
