package org.uj.project.ensembleheart.neuralnet;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author fchidzikwe
 */

@Data
public class InputLayer implements Serializable {

    private static final long serialVersionUID = 6529685098267757692L;
    List<Neuron> neurons = new ArrayList<>();

    InputLayer(int numberOfNeurons){
        //input neurons
        for(int i=0;i<numberOfNeurons;i++){
            Neuron neuron = new Neuron();
            neurons.add(neuron);
        }

    }


    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("InputLayer{");
        sb.append("neurons=").append(neurons);
        sb.append('}');
        return sb.toString();
    }
}
