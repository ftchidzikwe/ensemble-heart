package org.uj.project.ensembleheart.neuralnet;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author fchidzikwe
 */

@Data
public class Neuron implements Serializable {

    private static final long serialVersionUID = 6529685098267757670L;

    private double output;

    private double error;


    Connection biasConn;

    private List<Connection> incomingConnections = new ArrayList<>();


    public void  calculateSignal(){
        double sum =0;

        for(Connection connection: incomingConnections){

         sum= sum +  connection.getWeight() * connection.getNeuronFrom().getOutput();
        }

        //adding bias
        sum = sum + biasConn.getWeight() * biasConn.getNeuronFrom().getOutput();
        output =   sigmoidActivationFunction(sum);
    }

    private double sigmoidActivationFunction(double input) {
        return 1d / (1 + Math.exp(-input));
    }

    public double getDerivative() {
        return output* (1.0 -output);
    }






}
