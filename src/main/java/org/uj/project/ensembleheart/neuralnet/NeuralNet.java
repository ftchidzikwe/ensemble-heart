package org.uj.project.ensembleheart.neuralnet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uj.project.ensembleheart.constants.Data;
import org.uj.project.ensembleheart.model.DataSet;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author fchidzikwe
 */


public class NeuralNet implements Serializable {

    private static final long serialVersionUID = 6529685098267757690L;

    private Logger logger = LoggerFactory.getLogger(NeuralNet.class);
    private InputLayer inputLayer = null;
    private HiddenLayer hiddenLayer = null;
    private OutputLayer outputLayer = null;


    final double learningRate = 0.3;
   //final double momentum = 0.7;
    private double[] output;


    public NeuralNet(int numberOfInputNeurons, int numberOfHiddenNeurons, int numberOfOutputNeurons) {
        inputLayer = new InputLayer(numberOfInputNeurons);
        hiddenLayer = new HiddenLayer(numberOfHiddenNeurons);
        outputLayer = new OutputLayer(numberOfOutputNeurons);

        //connections between neurons from layer to layer
        //1. input connections
        List<Connection> inputNeuronConnectionList = new ArrayList<>();
        //2. hidden conections
        List<Connection> hiddenNeuronConnectionList = new ArrayList<>();
        //3. output connections
        List<Connection> outputNeuronConnectionList = new ArrayList<>();


        //construct connections for input layer
        for (Neuron inputNeuron : inputLayer.getNeurons()) {
            Connection connection = new Connection();
            connection.setNeuronFrom(null);
            connection.setNeuronTo(inputNeuron);
            inputNeuronConnectionList.add(connection);
            inputNeuron.setIncomingConnections(inputNeuronConnectionList);

        }

        //2. hidden neurons- construct hidden layer connections
        for (Neuron hiddenNeuron : hiddenLayer.getNeurons()) {

            //each hidden neuron is connected to each neuron in input layer
            for (Neuron inputNeuron : inputLayer.getNeurons()) {
                Connection hiddenNueronConnection = new Connection();
                hiddenNueronConnection.setNeuronFrom(inputNeuron);
                hiddenNueronConnection.setNeuronTo(hiddenNeuron);
                hiddenNeuronConnectionList.add(hiddenNueronConnection);
            }


            Connection bConnection = new Connection(1, hiddenNeuron);
            hiddenNeuron.setBiasConn(bConnection);


            hiddenNeuron.setIncomingConnections(hiddenNeuronConnectionList);

        }


        //3. output neurons
        for (Neuron outputNeuron : outputLayer.getNeurons()) {

            //each output neuron is connected to each neuron in hidden layer
            for (Neuron hiddenNeuron : hiddenLayer.getNeurons()) {
                Connection hiddenToOutput = new Connection();
                hiddenToOutput.setNeuronFrom(hiddenNeuron);
                hiddenToOutput.setNeuronTo(outputNeuron);
                outputNeuronConnectionList.add(hiddenToOutput);
            }

            Connection bConnection = new Connection(1, outputNeuron);
            outputNeuron.setBiasConn(bConnection);


            outputNeuron.setIncomingConnections(outputNeuronConnectionList);
        }

    }

    public Double []  startTrainingToError(DataSet dataSet, double acceptedError, int iterations) throws IllegalAccessException {


        Double errorArray[] = new Double[iterations];
        double [][] trainingData = new double[][]{
                new double[]{0,0},
                new double[]{0,1},
                new double[]{1,0},
                new double[]{1,1}

        };


        double traingResult[][] = new double[][]{

                        new double[]{0},
                        new double[]{1},
                        new double[]{1},
                        new double[]{0},
        };



//           double error = new double[outputLayer.getNeurons().size()];



              for(int iteration =0; iteration< iterations; iteration++){

                  double iterationError=0;


                  System.out.println("============================= ITERATION "+ iteration + " =======================") ;
                  for (int i = 0; i <dataSet.getHeartDataSet().size(); i++) {

                      double input[] = new double[dataSet.getHeartDataSet().get(i).getInputArray().length], desired[] = new double[dataSet.getHeartDataSet().get(i).getOutputArray().length];
                      System.arraycopy(dataSet.getHeartDataSet().get(i).getInputArray(), 0, input, 0, dataSet.getHeartDataSet().get(i).getInputArray().length);
                      System.arraycopy(dataSet.getHeartDataSet().get(i).getOutputArray(), 0, desired, 0, dataSet.getHeartDataSet().get(i).getOutputArray().length);
                      output = forwardPass(input);
                      double error  = backwardPass(desired);
                      System.out.println( "============= desired  "+ Arrays.toString(desired)+   " output "+ Arrays.toString(output) );

                      error += error(output, desired);

                      iterationError+= error;

                  }

                  double epochError = iterationError/ dataSet.getHeartDataSet().size();

                  errorArray[iteration] = epochError;

                  System.out.println("============================= END OF ITERATION "+ iteration + " ======================= error = "+ epochError
                  ) ;

              }


              return  Arrays.copyOf(errorArray,iterations );
    }

    private double error(double[] output, double[] desired) {
        if (output.length != desired.length) {
            throw new IllegalArgumentException("The lengths of the actual and expected value arrays must be equal");
        }

        double sum = 0;

        for (int i = 0; i < desired.length; i++) {
            sum += Math.pow(desired[i] - output[i], 2);
        }

        return sum / 2;
    }

    public double [] forwardPass(double... inputArray) {
        //input neurons
        for (int i = 0; i < inputArray.length; i++) {
            //output for input neurons is the input signals, ie no processing
            inputLayer.getNeurons().get(i).setOutput(inputArray[i]);
        }

        //hidden neurons
        hiddenLayer.getNeurons().stream().forEach(Neuron::calculateSignal);

        //output neurons
        outputLayer.getNeurons().stream().forEach(Neuron::calculateSignal);

        //get output array

        double results[] = new double[outputLayer.getNeurons().size()];

        for (int i =0;i< outputLayer.getNeurons().size(); i++){
            results[i] = outputLayer.getNeurons().get(i).getOutput();

        }


        return Arrays.copyOf(results, results.length);
    }



    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("NeuralNet{");
        sb.append("inputLayer=").append(inputLayer);
        sb.append(", hiddenLayer=").append(hiddenLayer);
        sb.append(", outputLayer=").append(outputLayer);
        sb.append(", learningRate=").append(learningRate);
        sb.append('}');
        return sb.toString();
    }

    private double backwardPass(double... target) {


        double iterationErrorTotal = 0;

        for (int i = 0; i < outputLayer.getNeurons().size(); i++) {

            //claculate errors
            Neuron oNeuron =  outputLayer.getNeurons().get(i);
            double error = (target[i]  - oNeuron.getOutput() ) * oNeuron.getDerivative();
            oNeuron.setError(error);

            //update weights - hidden to output
            Connection biasConn = oNeuron.getBiasConn();
            double biasDelta = learningRate * oNeuron.getError();
            biasConn.setWeight(biasConn.getWeight() + biasDelta);

            for (Connection hiddenToOutputConnection : oNeuron.getIncomingConnections()) {
                double delta = learningRate * oNeuron.getError() * hiddenToOutputConnection.getNeuronFrom().getOutput() ;
                double newWeight = hiddenToOutputConnection.getWeight() + delta;
                hiddenToOutputConnection.setWeight(newWeight);
            }

        }

        iterationErrorTotal += calculateRMSError(target);

        //calculate hidden neurons errors
        for (int i = 0; i < hiddenLayer.getNeurons().size(); i++) {
            Neuron hNeuron = hiddenLayer.getNeurons().get(i);

            double hNeuronError =0.0;

            //calculating hidden errors
            for(int j =0;j< outputLayer.getNeurons().size(); j++){
                Neuron oNeuron = outputLayer.getNeurons().get(j);

                for(Connection hiddenToOutputConn : oNeuron.getIncomingConnections()){

                    if(hiddenToOutputConn.getNeuronFrom()==hNeuron){
                        hNeuronError =  hNeuronError + hiddenToOutputConn.getWeight() * oNeuron.getError() * hNeuron.getDerivative();
                    }
                }
            }
            hNeuron.setError(hNeuronError);



            //updating weights - input to hidden weights
            Connection biasConn = hNeuron.getBiasConn();
            double biasDelta = learningRate * hNeuron.getError();
            biasConn.setWeight(biasConn.getWeight() + biasDelta);

            for (Connection inputToHiddenConn : hNeuron.getIncomingConnections()) {
                double delta = learningRate * hNeuron.getError() * inputToHiddenConn.getNeuronFrom().getOutput() ;
                double newWeight = inputToHiddenConn.getWeight() + delta;
                inputToHiddenConn.setWeight(newWeight);
            }
        }



        //sum the errors for each output neuron


        return  iterationErrorTotal;
    }

    private double calculateRMSError(double[] target) {

        double sum = 0;

        double[] actual = new double[outputLayer.getNeurons().size()];

        for(int i =0; i< outputLayer.getNeurons().size();i++){
            actual[i] = outputLayer.getNeurons().get(i).getOutput();
        }
        for (int i = 0; i < target.length; i++) {
            sum += Math.pow(target[i] - actual[i], 2);
        }

        return sum / 2;
    }


    public void initialiseWeights() {
        // initialise input weight
        inputLayer.getNeurons().stream().forEach(neuron -> {
            neuron.getIncomingConnections().stream().forEach(connection -> {
                connection.setWeight( (Math.random() * 1) - 0.5);
            });

        });

        //initialise input - hidden weights
        hiddenLayer.getNeurons().stream().forEach(neuron -> {
            neuron.getIncomingConnections().stream().forEach( connection -> {
                connection.setWeight((Math.random() * 1) - 0.5);
            });
            neuron.getBiasConn().setWeight((Math.random() * 1) - 0.5);
        });

        //initialize hidden- output
        outputLayer.getNeurons().stream().forEach(neuron -> {
            neuron.getIncomingConnections().stream().forEach(connection -> {
                connection.setWeight(0.5 - Math.random());
            });
            neuron.getBiasConn().setWeight(0.5 - Math.random());
        });
    }


    public void saveTestedNeuralNetwork(NeuralNet trainedNeuralNetwork) {
        try {
            FileOutputStream fout = new FileOutputStream(Data.pathToTestedNN());
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(trainedNeuralNetwork);
            fout.close();
            oos.close();

        } catch (IOException e) {
             e.printStackTrace();

        }
    }

    public NeuralNet retrieveTrainedNN() {
        NeuralNet neuralNetwork = null;
        try {
            FileInputStream fileIn = new FileInputStream(Data.pathToTrainedNN());
            ObjectInputStream in = new ObjectInputStream(fileIn);
            neuralNetwork = (NeuralNet) in.readObject();
            in.close();
            fileIn.close();
        } catch (IOException i) {
            i.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return neuralNetwork;
    }


    public static NeuralNet retrieveTestedNN() {
        NeuralNet neuralNetwork = null;
        try {
            FileInputStream fileIn = new FileInputStream(Data.pathToTestedNN());
            ObjectInputStream in = new ObjectInputStream(fileIn);
            neuralNetwork = (NeuralNet) in.readObject();
            in.close();
            fileIn.close();
        } catch (IOException i) {
            i.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return neuralNetwork;
    }
}
