package org.uj.project.ensembleheart.neuralnet;

/**
 * @author fchidzikwe
 */
public class NodeView {


    public final String question;
    public final double entropy;
    public final int dataSize;


    public final int height;
    public final int width;

    public NodeView(String question, double entropy, int dataSize) {
        this.question = question;
        this.entropy = entropy;
        this.dataSize = dataSize;
        this.height = 80;
        this.width = 80;
    }
}


