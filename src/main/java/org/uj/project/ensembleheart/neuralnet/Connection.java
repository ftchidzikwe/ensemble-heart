package org.uj.project.ensembleheart.neuralnet;

import lombok.Data;

import java.io.Serializable;
import java.util.Random;

/**
 * @author fchidzikwe
 */

@Data
public class Connection implements Serializable {

    private static final long serialVersionUID = 6529685098267757693L;
//    double prevDeltaWeight = 0; // for momentum
    double deltaWeight = 0;

    final Random randonNumberGenerater = new Random();

    private double weight;

    private Neuron neuronFrom;

    private Neuron neuronTo;


    Connection(){

    }

    //bias connection
    public Connection(double bias, Neuron neuronTo) {
        Neuron b = new Neuron();
        b.setOutput(bias);
        this.neuronFrom =b;
        this.neuronTo = neuronTo;
    }



    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Connection{");
        sb.append("weight=").append(weight);
        sb.append('}');
        return sb.toString();
    }
}
