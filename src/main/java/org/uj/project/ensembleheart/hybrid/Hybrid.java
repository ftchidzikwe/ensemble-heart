package org.uj.project.ensembleheart.hybrid;

import org.uj.project.ensembleheart.constants.Data;
import org.uj.project.ensembleheart.model.DataSet;
import org.uj.project.ensembleheart.util.CsvOps;

import java.io.IOException;

/**
 * @author fchidzikwe
 */
public class Hybrid {



    // takes results of weighted average and return the argmax
    public double makePredction(double [] input, double [] accuracy){

        double nnWeightedResult =  input[0] * accuracy[0];

        double dtWeightedResult =input[0] * accuracy[0];


        double average  =  (nnWeightedResult + dtWeightedResult)/2;



        return average;

    }


    // this method will train the nn dt and hybrid
    public String training(DataSet dataSet) throws IOException {

        CsvOps csvOps = new CsvOps();

        DataSet dtTrainingDataSet = csvOps.readCsvAndParseToObject(Data.getDataFileLocation());


        return null;
    }
    


}
