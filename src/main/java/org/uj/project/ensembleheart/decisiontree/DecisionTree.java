package org.uj.project.ensembleheart.decisiontree;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uj.project.ensembleheart.model.HeartData;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author fchidzikwe
 */
public class DecisionTree {

    Logger logger = LoggerFactory.getLogger(DecisionTree.class);


    int numberOfTimesCallingTraining = 0;

    Set<Integer> usedKeySet = new HashSet<>();


    public void startTraining(double purityNumber, Stack<Node> tree, Stack<Node> printTree) throws IllegalAccessException {

        while(!tree.isEmpty()) {

            Node node = tree.pop();

            numberOfTimesCallingTraining++;
            logger.error("calling start training for the  = " + numberOfTimesCallingTraining);
            Map<Integer, Double> splitPoint = getBestSplitColumn(node);


            logger.error("=============the node spliting point is " + splitPoint);

            Integer columnIndex = splitPoint.keySet().stream().findFirst().get();
             String columnName = getColumnStringValue(columnIndex);

            Double value = splitPoint.get(columnIndex);

            Question question = new Question(columnIndex, columnName, value);
            node.setQuestion(question);


                //if  we keep finding key
                if (!columnName.equalsIgnoreCase("notfound")) {

                    List<HeartData> pointsBelow = new ArrayList<>();
                    List<HeartData> pointsAbove = new ArrayList<>();
                    Node nodeBelow, nodeAbove;
                    logger.error("++++++++++++++++++++++SPLITTING BY" + columnName + "++++++++++++++++++++++++++" + ": " + value);
                    switch (columnName) {
                        case "age":
                            // split value is age
                            pointsBelow = node.getHeartDataList().stream().filter(d -> d.getAge() <= value).collect(Collectors.toList());
                            pointsAbove = node.getHeartDataList().stream().filter(d -> d.getAge() > value).collect(Collectors.toList());
                            node.setVisited(true);
                            node.setEntropy(calculateOverallEntropy(pointsBelow, pointsAbove));
                            //create children
                            nodeBelow = new Node(pointsBelow);
                            nodeAbove = new Node(pointsAbove);
                            node.getChildren()[0] = nodeBelow.getNodeId();
                            node.getChildren()[1] = nodeAbove.getNodeId();

                            nodeAbove.setParent(node);
                            nodeBelow.setParent(node);

                            processNodes(nodeBelow, nodeAbove, tree,printTree,purityNumber);

                            break;

                        case "sex":
                            // split value is sex
                            pointsBelow = node.getHeartDataList().stream().filter(d -> d.getSex() <= value).collect(Collectors.toList());
                            pointsAbove = node.getHeartDataList().stream().filter(d -> d.getSex() > value).collect(Collectors.toList());
                            node.setVisited(true);
                            node.setEntropy(calculateOverallEntropy(pointsBelow, pointsAbove));
                            //create children
                            nodeBelow = new Node(pointsBelow);
                            nodeAbove = new Node(pointsAbove);
                            node.getChildren()[0] = nodeBelow.getNodeId();
                            node.getChildren()[1] = nodeAbove.getNodeId();
                            nodeAbove.setParent(node);
                            nodeBelow.setParent(node);
                            processNodes(nodeBelow, nodeAbove, tree,printTree,purityNumber);
                            break;

                        case "cp":
                            // split value is cp
                            pointsBelow = node.getHeartDataList().stream().filter(d -> d.getCp() <= value).collect(Collectors.toList());
                            pointsAbove = node.getHeartDataList().stream().filter(d -> d.getCp() > value).collect(Collectors.toList());
                            node.setVisited(true);
                            node.setEntropy(calculateOverallEntropy(pointsBelow, pointsAbove));
                            //create children
                            nodeBelow = new Node(pointsBelow);
                            nodeAbove = new Node(pointsAbove);
                            node.getChildren()[0] = nodeBelow.getNodeId();
                            node.getChildren()[1] = nodeAbove.getNodeId();

                            nodeAbove.setParent(node);
                            nodeBelow.setParent(node);
                            processNodes(nodeBelow, nodeAbove, tree,printTree,purityNumber);

                            break;

                        case "trestbps":
                            // split value is trestbps
                            pointsBelow = node.getHeartDataList().stream().filter(d -> d.getTrestbps() <= value).collect(Collectors.toList());
                            pointsAbove = node.getHeartDataList().stream().filter(d -> d.getTrestbps() > value).collect(Collectors.toList());
                            node.setVisited(true);
                            node.setEntropy(calculateOverallEntropy(pointsBelow, pointsAbove));
                            //create children
                            nodeBelow = new Node(pointsBelow);
                            nodeAbove = new Node(pointsAbove);
                            node.getChildren()[0] = nodeBelow.getNodeId();
                            node.getChildren()[1] = nodeAbove.getNodeId();

                            nodeAbove.setParent(node);
                            nodeBelow.setParent(node);
                            //order matters
                            processNodes(nodeBelow, nodeAbove, tree,printTree,purityNumber);
                            break;

                        case "chol":
                            // split value is trestbps
                            pointsBelow = node.getHeartDataList().stream().filter(d -> d.getChol() <= value).collect(Collectors.toList());
                            pointsAbove = node.getHeartDataList().stream().filter(d -> d.getChol() > value).collect(Collectors.toList());
                            node.setVisited(true);
                            node.setEntropy(calculateOverallEntropy(pointsBelow, pointsAbove));
                            //create children
                            nodeBelow = new Node(pointsBelow);
                            nodeAbove = new Node(pointsAbove);
                            node.getChildren()[0] = nodeBelow.getNodeId();
                            node.getChildren()[1] = nodeAbove.getNodeId();

                            nodeAbove.setParent(node);
                            nodeBelow.setParent(node);

                            //order matters
                            processNodes(nodeBelow, nodeAbove, tree,printTree,purityNumber);

                            break;

                        case "fbs":
                            // split value is fbs
                            pointsBelow = node.getHeartDataList().stream().filter(d -> d.getFbs() <= value).collect(Collectors.toList());
                            pointsAbove = node.getHeartDataList().stream().filter(d -> d.getFbs() > value).collect(Collectors.toList());
                            node.setVisited(true);
                            node.setEntropy(calculateOverallEntropy(pointsBelow, pointsAbove));
                            //create children
                            nodeBelow = new Node(pointsBelow);
                            nodeAbove = new Node(pointsAbove);
                            node.getChildren()[0] = nodeBelow.getNodeId();
                            node.getChildren()[1] = nodeAbove.getNodeId();

                            nodeAbove.setParent(node);
                            nodeBelow.setParent(node);
                            //order matters
                            processNodes(nodeBelow, nodeAbove, tree,printTree,purityNumber);

                            break;


                        case "restecg":
                            // split value is restecg
                            pointsBelow = node.getHeartDataList().stream().filter(d -> d.getRestecg() <= value).collect(Collectors.toList());
                            pointsAbove = node.getHeartDataList().stream().filter(d -> d.getRestecg() > value).collect(Collectors.toList());
                            node.setVisited(true);
                            node.setEntropy(calculateOverallEntropy(pointsBelow, pointsAbove));
                            //create children
                            nodeBelow = new Node(pointsBelow);
                            nodeAbove = new Node(pointsAbove);

                            node.getChildren()[0] = nodeBelow.getNodeId();
                            node.getChildren()[1] = nodeAbove.getNodeId();
                            nodeAbove.setParent(node);
                            nodeBelow.setParent(node);
                            //order matters
                            processNodes(nodeBelow, nodeAbove, tree,printTree,purityNumber);

                            break;

                        case "thalach":
                            // split value is thalach
                            pointsBelow = node.getHeartDataList().stream().filter(d -> d.getThalach() <= value).collect(Collectors.toList());
                            pointsAbove = node.getHeartDataList().stream().filter(d -> d.getThalach() > value).collect(Collectors.toList());
                            node.setVisited(true);
                            node.setEntropy(calculateOverallEntropy(pointsBelow, pointsAbove));
                            //create children
                            nodeBelow = new Node(pointsBelow);
                            nodeAbove = new Node(pointsAbove);
                            node.getChildren()[0] = nodeBelow.getNodeId();
                            node.getChildren()[1] = nodeAbove.getNodeId();

                            nodeAbove.setParent(node);
                            nodeBelow.setParent(node);
                            //order matters
                            processNodes(nodeBelow, nodeAbove, tree,printTree,purityNumber);
                            break;


                        case "exang":
                            // split value is exang
                            pointsBelow = node.getHeartDataList().stream().filter(d -> d.getExang() <= value).collect(Collectors.toList());
                            pointsAbove = node.getHeartDataList().stream().filter(d -> d.getExang() > value).collect(Collectors.toList());
                            node.setVisited(true);
                            node.setEntropy(calculateOverallEntropy(pointsBelow, pointsAbove));
                            //create children
                            nodeBelow = new Node(pointsBelow);
                            nodeAbove = new Node(pointsAbove);
                            node.getChildren()[0] = nodeBelow.getNodeId();
                            node.getChildren()[1] = nodeAbove.getNodeId();

                            nodeAbove.setParent(node);
                            nodeBelow.setParent(node);
                            //order matters
                            processNodes(nodeBelow, nodeAbove, tree,printTree,purityNumber);

                            break;


                        case "oldpeak":
                            // split value is oldpeak
                            pointsBelow = node.getHeartDataList().stream().filter(d -> d.getOldpeak() <= value).collect(Collectors.toList());
                            pointsAbove = node.getHeartDataList().stream().filter(d -> d.getOldpeak() > value).collect(Collectors.toList());
                            node.setVisited(true);
                            node.setEntropy(calculateOverallEntropy(pointsBelow, pointsAbove));
                            //create children
                            nodeBelow = new Node(pointsBelow);
                            nodeAbove = new Node(pointsAbove);
                            node.getChildren()[0] = nodeBelow.getNodeId();
                            node.getChildren()[1] = nodeAbove.getNodeId();

                            nodeAbove.setParent(node);
                            nodeBelow.setParent(node);
                            //order matters
                            processNodes(nodeBelow, nodeAbove, tree,printTree,purityNumber);
                            break;

                        case "slope":
                            // split value is slope
                            pointsBelow = node.getHeartDataList().stream().filter(d -> d.getSlope() <= value).collect(Collectors.toList());
                            pointsAbove = node.getHeartDataList().stream().filter(d -> d.getSlope() > value).collect(Collectors.toList());
                            node.setVisited(true);
                            node.setEntropy(calculateOverallEntropy(pointsBelow, pointsAbove));
                            //create children
                            nodeBelow = new Node(pointsBelow);
                            nodeAbove = new Node(pointsAbove);

                            node.getChildren()[0] = nodeBelow.getNodeId();
                            node.getChildren()[1] = nodeAbove.getNodeId();
                            nodeAbove.setParent(node);
                            nodeBelow.setParent(node);
                            //order matters
                            processNodes(nodeBelow, nodeAbove, tree,printTree,purityNumber);
                            break;


                        case "ca":
                            // split value is ca
                            pointsBelow = node.getHeartDataList().stream().filter(d -> d.getCa() <= value).collect(Collectors.toList());
                            pointsAbove = node.getHeartDataList().stream().filter(d -> d.getCa() > value).collect(Collectors.toList());
                            //create children
                            nodeBelow = new Node(pointsBelow);
                            nodeAbove = new Node(pointsAbove);
                            nodeAbove.setParent(node);
                            nodeBelow.setParent(node);


                            node.setVisited(true);
                            node.setEntropy(calculateOverallEntropy(pointsBelow, pointsAbove));
                            node.getChildren()[0] = nodeBelow.getNodeId();
                            node.getChildren()[1] = nodeAbove.getNodeId();


                            //order matters
                            processNodes(nodeBelow, nodeAbove, tree,printTree,purityNumber);
                            break;

                        case "thal":
                            // split value is thal
                            pointsBelow = node.getHeartDataList().stream().filter(d -> d.getThal() <= value).collect(Collectors.toList());
                            pointsAbove = node.getHeartDataList().stream().filter(d -> d.getThal() > value).collect(Collectors.toList());

                            nodeBelow = new Node(pointsBelow);
                            nodeAbove = new Node(pointsAbove);
                            nodeAbove.setParent(node);
                            nodeBelow.setParent(node);


                            node.setVisited(true);
                            node.setEntropy(calculateOverallEntropy(pointsBelow, pointsAbove));
                            node.getChildren()[0] = nodeBelow.getNodeId();
                            node.getChildren()[1] = nodeAbove.getNodeId();
                            processNodes(nodeBelow, nodeAbove, tree,printTree,purityNumber);

                            break;
                    }

            }
        }
    }

    private void processNodes(Node nodeBelow, Node nodeAbove,  Stack<Node> tree, Stack<Node> printTree, double purityNumber) throws IllegalAccessException {
        // FOR NODE BELOW
        checkNode(nodeBelow , tree, printTree,purityNumber);
        //FOR NODE ABOVE
        checkNode(nodeAbove , tree, printTree,purityNumber);

    }

    private void checkNode(Node node, Stack<Node> tree, Stack<Node> printTree, double purityNumber) throws IllegalAccessException {

        if(!isPure(node, purityNumber) ){ // node is impure add it to tree and print tree and train
            printTree.push(node);
            tree.push(node);
            startTraining(purityNumber,tree, printTree );
        }else { // node is pure add it print tree
            printTree.push(node);
        }
    }

    public Boolean isPure(Node node, double purityPercentage) {

        //take dataset of that node

        logger.error("=======checking purity of node "+ node.getNodeId() +"============");

        List<HeartData> data = node.getHeartDataList();

        //data size
        int dataSize = data.size();

        double zeroCount =  data.stream().filter(heartData -> heartData.getTarget()==0).count();
        double oneCount = data.stream().filter(heartData -> heartData.getTarget()==1).count();


        logger.error("ones = " + oneCount);
        logger.error("zeros = " + zeroCount);
        double zeroPercentage = zeroCount / dataSize;
        logger.error("zeroPercentage = " + zeroPercentage);
        double onePercentage = oneCount / dataSize;
        logger.error("onePercentage = " + onePercentage);
        double acceptablePurityNumber = Math.floor(purityPercentage * dataSize);
        logger.error("acceptablePurityNumber = " + acceptablePurityNumber);


        if ((zeroCount >= acceptablePurityNumber) || (oneCount >= acceptablePurityNumber)) {

            node.setLeaf(true);
            return Boolean.TRUE;
        } else {
            node.setLeaf(false);
            return Boolean.FALSE;
        }

    }

    public Map<Integer, Set<Double>> getPotentialSplits(Node node) throws IllegalAccessException {


        Map<Integer, Set<Double>> potentialSplits = new HashMap<>();

        DTUtil dtUtil = new DTUtil();

        double[][] nodeData = dtUtil.convertListToArray(node.getHeartDataList());


        for (int r = 0; r < 13; r++) { // loop thru number of columns = 13

            double[] columnValues = new double[nodeData.length];

            for (int c = 0; c < nodeData.length; c++) { // loop number of   rows  = 303

                columnValues[c] = nodeData[c][r];

            }

            Set<Double> middlePoints = new HashSet<>();
            double[] unique = Arrays.stream(columnValues).distinct().sorted().toArray();

            // loop thru unique
            for (int i = 0; i < unique.length; i++) {
                if (i != 0) {
                    double curr = unique[i];
                    double prev = unique[i - 1];
                    double middle = (curr + prev) / 2;
                    middlePoints.add(middle);

                }
            }


            potentialSplits.put(r, middlePoints);


        }

        return potentialSplits;
    }

    private double getSplitValue(Double curr, Double next) {

        return (curr + next) / 2;
    }

    public double calculateEntropy(List<HeartData> data) {

        int datasetSize = data.size();
        double dataEntropy = 0;

        // todo investigate on NaN why obect doesnt have 1 and 0s in target


        if (datasetSize == 0)
            return datasetSize;
        //get positive count for heart

        long positiveCount = data.stream().filter(heartData -> 1 == heartData.getTarget()).count();
        long negativeCount = data.stream().filter(heartData -> 0 == heartData.getTarget()).count();

        double positiveProb = (double) positiveCount / datasetSize;
        double negativeProb = (double) negativeCount / datasetSize;

        if (positiveProb == 0 || negativeProb == 0) {

            return dataEntropy;

        } else {
            dataEntropy = (-1 * positiveProb * (Math.log(positiveProb) / (Math.log(2))) + (-1 * negativeProb * (Math.log(negativeProb) / Math.log(2))));
        }


        logger.error("==========negativeCount = " + negativeCount);
        logger.error("==========positiveCount = " + positiveCount);
        logger.error("==========datasetSize = " + datasetSize);
        logger.error("==========positiveProb = " + positiveProb);
        logger.error("==========negativeProb = " + negativeProb);
        logger.error("==========dataEntropy = " + dataEntropy);
        return dataEntropy;
    }

    public double calculateOverallEntropy(List<HeartData> below, List<HeartData> above) {

        double sizeOfDataBelow = below.size();
        double sizeOfDataAbove = above.size();
        double totalSizeOfDataset = sizeOfDataBelow + sizeOfDataAbove;

        double probabiltyOfDataBelow = sizeOfDataBelow / totalSizeOfDataset;
        double probabiltyOfDataAbove = sizeOfDataAbove / totalSizeOfDataset;


        double overallEntropy = (probabiltyOfDataBelow * calculateEntropy(below)) + (probabiltyOfDataAbove * calculateEntropy(above));

        logger.error("######################### entropy below " + calculateEntropy(below));
        logger.error("######################### entropy above " + calculateEntropy(above));
        logger.error("######################### overallentropy " + overallEntropy);
        return overallEntropy;

    }


    public Map<Integer, Double> getBestSplitColumn(Node node) throws IllegalAccessException {
        Map<Integer, Set<Double>> middleSplitsPoint = getPotentialSplits(node);
        double minValue = 1000000;
        Integer pointKey = 1000000;
        double lowestOverallEntropy = 100000;

        for (Integer key : middleSplitsPoint.keySet()) {
            // for each potential split get the one with lowest entropy
            Set<Double> values = middleSplitsPoint.get(key);
            for (Double value : values) {
                //calculate entropy with  current value
                List<HeartData> pointsBelow = node.getHeartDataList().stream().filter(d -> d.getAge() <= value).collect(Collectors.toList());
                List<HeartData> pointsAbove = node.getHeartDataList().stream().filter(d -> d.getAge() > value).collect(Collectors.toList());

                double currentEntropy = calculateOverallEntropy(pointsBelow, pointsAbove);


                logger.error(" @@@@@@@@@@@@@@@@@@@ node " + node.getNodeId() + "||  entropy " + currentEntropy);

                if (currentEntropy < lowestOverallEntropy) {

                    if (!usedKeySet.contains(key)) {
                        minValue = value;
                        pointKey = key;
                        usedKeySet.add(key);
                        lowestOverallEntropy = currentEntropy;
                        usedKeySet.add(key);
                    }

                }


            }
        }

        Map map = new HashMap();
        map.put(pointKey, minValue);
        return map;
    }


    public double testDecistionTree(double input [], Stack<Node> trainedTree){
        // getting questions from trained node
        List<Node> nodeList = new ArrayList<>(trainedTree);
        //get root node
        Node node = nodeList.get(0);
        return  askQuestions(node, input, nodeList);
    }


    double askQuestions(Node node, double[] input, List<Node> list){



        Node nextNode = null;
        Question question = node.getQuestion();

        int firstChildId = node.getChildren()[0];

        int secondChildId = node.getChildren()[1];


        // node maybe be leaf
        if(node.isLeaf() ==true){
            long ones = node.getHeartDataList().stream().filter(d -> d.getTarget() ==1).count();
            long zeros = node.getHeartDataList().stream().filter(d -> d.getTarget() ==0).count();

            if(ones > zeros){
                return 1;
            }else{
                return  0;
            }
        }else{
            // node is not leaf

            if(question==null || question.getColumnName().equalsIgnoreCase("notfound")){
                // is a leaf node
                //for that node count the class  which is dominate

                long ones = node.getHeartDataList().stream().filter(d -> d.getTarget() ==1).count();
                long zeros = node.getHeartDataList().stream().filter(d -> d.getTarget() ==0).count();

                if(ones > zeros){
                    return 1;
                }else{
                    return  0;
                }

            }else {
                //else ask question
                int questionIndex = question.getColumnIndex();
                double value =   question.getValue();

                if(input[questionIndex] < value){

                    // getting next node search in list for node with id = firstChildId
                    nextNode = list.stream().filter(n-> n.getNodeId() == firstChildId).findFirst().get();


                }else {
                    nextNode = list.stream().filter(n-> n.getNodeId() == secondChildId).findFirst().get();

                }


                list.remove(node);
               return askQuestions(nextNode,input, list);

            }
        }


    }


    String getColumnStringValue(int index){
        String columnKeyStringValue = "";
        switch (index) {
            case 0: //age
                columnKeyStringValue = "age";
                break;
            case 1: //sex
                columnKeyStringValue = "sex";
                break;
            case 2: //cp
                columnKeyStringValue = "cp";
                break;
            case 3: //trestbps
                columnKeyStringValue = "trestbps";
                break;
            case 4: //chol
                columnKeyStringValue = "chol";
                break;
            case 5: //fbs
                columnKeyStringValue = "fbs";
                break;
            case 6: //restecg
                columnKeyStringValue = "restecg";
                break;
            case 7: //thalach
                columnKeyStringValue = "thalach";
                break;
            case 8: //exang
                columnKeyStringValue = "exang";
                break;
            case 9: //oldpeak
                columnKeyStringValue = "oldpeak";
                break;
            case 10: //slope
                columnKeyStringValue = "slope";
                break;
            case 11: //age
                columnKeyStringValue = "ca";
                break;
            case 12: //thal
                columnKeyStringValue = "thal";
                break;


            case 1000000:
                columnKeyStringValue = "notfound";
                break;

        }


       return  columnKeyStringValue;
    }
}

