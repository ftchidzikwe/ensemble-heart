package org.uj.project.ensembleheart.decisiontree;

/**
 * @author fchidzikwe
 */


public class VisiualiseTree {

   static StringBuffer  makeTree(Node parent, Node childBelow, Node childAbove){


       String parentLabel = makeLabels(parent);
       String childBelowLabel = makeLabels(childBelow);
       String childAboveLabel = makeLabels(childAbove);


       // now we need to label the arrows

      StringBuffer stringBuffer = new StringBuffer();

      stringBuffer.append(parentLabel ).append( "-> "). append(childBelowLabel);
      stringBuffer.append(System.lineSeparator());
       stringBuffer.append(parentLabel ).append( "-> "). append(childAboveLabel);

      return stringBuffer;
   }

    private static String makeLabels(Node node) {
       String nodeName;
       if(node.getNodeId()==1){
           nodeName = "RootNode";
       }else if(node.isLeaf()){
           nodeName = "LeafNode " + node.getNodeId();
       }else {
           nodeName = "DecisiveNode " + node.getNodeId();
       }

        return  "Name "  + nodeName+ System.lineSeparator() +
                "DataSize :"+ node.getHeartDataList().size() + System.lineSeparator() +
                "Entropy :"+ node.getEntropy() + System.lineSeparator() +
                " Question :" + node.getQuestion();
    }


    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("VisiualiseTree{");
        sb.append('}');
        return sb.toString();
    }
}
