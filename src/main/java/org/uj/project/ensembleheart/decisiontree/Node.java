package org.uj.project.ensembleheart.decisiontree;

import lombok.Data;
import org.uj.project.ensembleheart.model.HeartData;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author fchidzikwe
 */

@Data
public class Node {

    private static final AtomicInteger count = new AtomicInteger(0);

    int nodeId;

    private Node parent;

    // each node has two children - yes child (index 0 ) and no child ( index 1)
    private int [] children = new int[2];

    private Question question;

    private boolean isVisited;


    private boolean isLeaf;

    private List<HeartData> heartDataList;

    private double entropy;


    private double gain;


    public  Node(List<HeartData> heartDataList){
        //set data to this node

        nodeId = count.incrementAndGet(); ;

        this.heartDataList = heartDataList;


    }


    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Node{");
        sb.append("id= ").append(nodeId);
        sb.append(" || ");
        sb.append("Question = ").append(question);
        sb.append(" || ");
        sb.append("Visited =").append(isVisited);
        sb.append(" || ");
        sb.append("Leaf Node =").append(isLeaf);
        sb.append(" || ");
        sb.append("Data size in node " + nodeId +  "  = "  ).append(heartDataList.size());
        sb.append(" || ");
        sb.append("Num of Ones  = ").append(heartDataList.stream().filter(heartData -> heartData.getTarget()==1).count());
        sb.append(" || ");
        sb.append("Num of zeros= ").append(heartDataList.stream().filter(heartData -> heartData.getTarget()==0).count());
        sb.append(" || ");
        sb.append("entropy = ").append(this.getEntropy());
        sb.append(System.lineSeparator());
        sb.append("\t");
        sb.append( "==========  CHILDREN OF NODE "+  nodeId +  " :: " + Arrays.toString(children) );
        sb.append(System.lineSeparator());
        sb.append(System.lineSeparator());
        sb.append(System.lineSeparator());
        sb.append('}');
        return sb.toString();
    }
}
