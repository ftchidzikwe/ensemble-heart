package org.uj.project.ensembleheart.decisiontree;

import org.uj.project.ensembleheart.model.HeartData;

import java.util.Arrays;
import java.util.List;

/**
 * @author fchidzikwe
 */
public class DTUtil {

    //convert list of heart data to 2 d array

    public double[][] convertListToArray(List<HeartData> heartDataList) throws IllegalAccessException {



        //number of rows
        int rows = heartDataList.size();

        int columns  = HeartData.class.getDeclaredFields().length;
        double [][] data = new double[rows][columns];

        for(int row =0; row< rows; row++){
            for(int col =0; col< columns; col++){

                data[row][col] = heartDataList.get(row).getRowArray()[col];
            }


        }


        return Arrays.copyOf(data, rows);

    }



    //convert 2d array  to list of heart Data
}
