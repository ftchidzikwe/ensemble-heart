package org.uj.project.ensembleheart.decisiontree;

/**
 * @author fchidzikwe
 */

import org.abego.treelayout.TreeForTreeLayout;
import org.abego.treelayout.util.DefaultTreeForTreeLayout;
import org.uj.project.ensembleheart.neuralnet.NodeView;


/**
 * Creates "Sample" trees, e.g. to be used in demonstrations.
 *
 * @author Udo Borkowski (ub@abego.org)
 */
public class SampleTreeFactory {

    /**
     * @return a "Sample" tree with {@link NodeView} items as nodes.
     */
    public static TreeForTreeLayout<NodeView> createSampleTree() {
        NodeView root = new NodeView("root", 80, 80);
        NodeView n1 = new NodeView("n1", 80, 80);
        NodeView n1_1 = new NodeView("n1.1\n(first node)", 80, 80);
        NodeView n1_2 = new NodeView("n1.2", 80, 80);
        NodeView n1_3 = new NodeView("n1.3\n(last node)", 80, 80);
        NodeView n2 = new NodeView("n2", 80, 80);
        NodeView n2_1 = new NodeView("n2", 80, 80);

        DefaultTreeForTreeLayout<NodeView> tree = new DefaultTreeForTreeLayout<NodeView>(
                root);
        tree.addChild(root, n1);
        tree.addChild(n1, n1_1);
        tree.addChild(n1, n1_2);
        tree.addChild(n1, n1_3);
        tree.addChild(root, n2);
        tree.addChild(n2, n2_1);
        return tree;
    }

    /**
     * @return a "Sample" tree with {@link NodeView} items as nodes.
     */
    public static TreeForTreeLayout<NodeView> createSampleTree2() {
        NodeView root = new NodeView("prog", 80, 80);
        NodeView n1 = new NodeView("classDef", 80, 200);
        NodeView n1_1 = new NodeView("class", 80, 200);
        NodeView n1_2 = new NodeView("T", 80, 200);
        NodeView n1_3 = new NodeView("{", 80, 200);
        NodeView n1_4 = new NodeView("member", 80, 200);
        NodeView n1_5 = new NodeView("member", 80, 200);
        NodeView n1_5_1 = new NodeView("<ERROR:int>", 80, 200);
        NodeView n1_6 = new NodeView("member", 80, 200);
        NodeView n1_6_1 = new NodeView("int", 80, 200);
        NodeView n1_6_2 = new NodeView("i", 80, 200);
        NodeView n1_6_3 = new NodeView(";", 80, 200);
        NodeView n1_7 = new NodeView("}", 80, 200);


        DefaultTreeForTreeLayout<NodeView> tree = new DefaultTreeForTreeLayout<NodeView>(
                root);
        tree.addChild(root, n1);
        tree.addChild(n1, n1_1);
        tree.addChild(n1, n1_2);
        tree.addChild(n1, n1_3);
        tree.addChild(n1, n1_4);
        tree.addChild(n1, n1_5);
        tree.addChild(n1_5, n1_5_1);
        tree.addChild(n1, n1_6);
        tree.addChild(n1_6,n1_6_1);
        tree.addChild(n1_6,n1_6_2);
        tree.addChild(n1_6,n1_6_3);
        tree.addChild(n1, n1_7);
        return tree;
    }
}