package org.uj.project.ensembleheart.decisiontree;

import lombok.Data;

/**
 * @author fchidzikwe
 */

@Data
public class Question {

    private String columnName;

    private int columnIndex;


    private double value;

    public Question(int columnIndex, String columnName, double value) {
        this.columnName = columnName;
        this.value = value;
        this.columnIndex = columnIndex;
    }


    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append(columnName);
        sb.append(" < ");
        sb.append(value);
        return sb.toString();
    }
}
