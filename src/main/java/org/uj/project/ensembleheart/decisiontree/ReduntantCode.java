package org.uj.project.ensembleheart.decisiontree;

/**
 * @author fchidzikwe
 */
public class ReduntantCode {

//    public void initialgetPotentialMetthod(Node node) {
//
//        //map containing column name and split value
//        Map potentialSplits = new HashMap();
//
//        AtomicReference<Map> splitColumnAndValue = new AtomicReference<>();
//
//
//        Double minEntropy = 99999.0;
//
//        List<HeartData> data = node.getHeartDataList();
//
//
//        //iterate over each coulumn
//        for (int i = 0; i < 14; i++) {
//
//            if (i == 0) {// first coulumn is age
//
//                List<Double> uniqueAgeList = new ArrayList<>();
//                List<Double> potentialAgeList = new ArrayList<>();
//                data.stream().map(sc -> sc.getAge()).distinct().sorted().forEach(uniqueAgeList::add);
//
//
//                System.out.println("uniqueAgeList = " + uniqueAgeList);
//
//                for (int j = 0; j < uniqueAgeList.size() - 1; j++) {
//
//                    double splitValue = getSplitValue(uniqueAgeList.get(j), uniqueAgeList.get(j + 1));
//
//                    DataSet dataAbove = new DataSet();
//                    DataSet dataBelow = new DataSet();
//                    dataAbove.setHeartDataSet(data.stream().filter(d -> d.getAge() > splitValue).collect(Collectors.toList()));
//                    dataBelow.setHeartDataSet(data.stream().filter(d -> d.getAge() <= splitValue).collect(Collectors.toList()));
//
//                    DataSet[] dataSetArray = {dataBelow, dataAbove};
//                    double currentEntropy = culculateOverallEntropy(dataSetArray);
//                    if (currentEntropy < minEntropy) {
//                        minEntropy = currentEntropy;
//                    }
//                    Map newMap = new HashMap();
//                    newMap.put("age", minEntropy);
//                    splitColumnAndValue.lazySet(newMap);
//
//                    potentialAgeList.add(getSplitValue(uniqueAgeList.get(j), uniqueAgeList.get(j + 1)));
//
//                }
//
//                System.out.println("potentialAgeList = " + potentialAgeList);
//
//                //for each potential list split data and calculate overall entropy
//
//
//                potentialSplits.put("age", potentialAgeList);
//
//            }
//
//            switch (i) {
//                case 0: // first column - age
//                    //get all list of age
//                    List<Double> uniqueAgeList = new ArrayList<>();
//                    List<Double> potentialAgeList = new ArrayList<>();
//                    data.stream().map(sc -> sc.getAge()).distinct().sorted().forEach(uniqueAgeList::add);
//
//
//                    System.out.println("uniqueAgeList = " + uniqueAgeList);
//
//                    for (int j = 0; j < uniqueAgeList.size() - 1; j++) {
//
//                        double splitValue = getSplitValue(uniqueAgeList.get(j), uniqueAgeList.get(j + 1));
//
//                        DataSet dataAbove = new DataSet();
//                        DataSet dataBelow = new DataSet();
//                        dataAbove.setHeartDataSet(data.stream().filter(d -> d.getAge() > splitValue).collect(Collectors.toList()));
//                        dataBelow.setHeartDataSet(data.stream().filter(d -> d.getAge() <= splitValue).collect(Collectors.toList()));
//
//                        DataSet[] dataSetArray = {dataBelow, dataAbove};
//                        double currentEntropy = culculateOverallEntropy(dataSetArray);
//                        if (currentEntropy < minEntropy) {
//                            minEntropy = currentEntropy;
//                        }
//                        Map newMap = new HashMap();
//                        newMap.put("age", minEntropy);
//                        splitColumnAndValue.lazySet(newMap);
//
//                        potentialAgeList.add(getSplitValue(uniqueAgeList.get(j), uniqueAgeList.get(j + 1)));
//
//                    }
//
//                    System.out.println("potentialAgeList = " + potentialAgeList);
//
//                    //for each potential list split data and calculate overall entropy
//
//
//                    potentialSplits.put("age", potentialAgeList);
//                    break;
//                case 1: // first column - sex
//                    //get all list of age
//                    List<Double> uniqueSexList = new ArrayList<>();
//                    data.stream().map(sc -> sc.getSex()).distinct().sorted().forEach(uniqueSexList::add);
//                    List<Double> potentialSexList = new ArrayList<>();
//                    for (int j = 0; j < uniqueSexList.size() - 1; j++) {
//
//                        double splitValue = getSplitValue(uniqueSexList.get(j), uniqueSexList.get(j + 1));
//                        DataSet dataAbove = new DataSet();
//                        DataSet dataBelow = new DataSet();
//                        dataAbove.setHeartDataSet(data.stream().filter(d -> d.getSex() > splitValue).collect(Collectors.toList()));
//                        dataBelow.setHeartDataSet(data.stream().filter(d -> d.getSex() <= splitValue).collect(Collectors.toList()));
//
//                        DataSet[] dataSetArray = {dataBelow, dataAbove};
//                        double currentEntropy = culculateOverallEntropy(dataSetArray);
//                        if (currentEntropy < minEntropy)
//                            minEntropy = currentEntropy;
//
//                        Map newMap = new HashMap();
//                        newMap.put("sex", minEntropy);
//                        splitColumnAndValue.lazySet(newMap);
//                        potentialSexList.add(getSplitValue(uniqueSexList.get(j), uniqueSexList.get(j + 1)));
//
//                    }
//
//                    potentialSplits.put("sex", potentialSexList);
//
//                    System.out.println("potentialSexList = " + potentialSexList);
//                    break;
//
//                case 2: // first column - cp
//                    //get all list of cp
//                    List<Double> uniqueCpList = new ArrayList<>();
//                    data.stream().map(sc -> sc.getCp()).distinct().sorted().forEach(uniqueCpList::add);
//                    List<Double> potentialCpList = new ArrayList<>();
//                    for (int j = 0; j < uniqueCpList.size() - 1; j++) {
//
//                        double splitValue = getSplitValue(uniqueCpList.get(j), uniqueCpList.get(j + 1));
//                        DataSet dataAbove = new DataSet();
//                        DataSet dataBelow = new DataSet();
//                        dataAbove.setHeartDataSet(data.stream().filter(d -> d.getCp() > splitValue).collect(Collectors.toList()));
//                        dataBelow.setHeartDataSet(data.stream().filter(d -> d.getCp() <= splitValue).collect(Collectors.toList()));
//
//                        DataSet[] dataSetArray = {dataBelow, dataAbove};
//                        double currentEntropy = culculateOverallEntropy(dataSetArray);
//                        if (currentEntropy < minEntropy)
//                            minEntropy = currentEntropy;
//
//                        Map newMap = new HashMap();
//                        newMap.put("cp", minEntropy);
//                        splitColumnAndValue.lazySet(newMap);
//                        potentialCpList.add(getSplitValue(uniqueCpList.get(j), uniqueCpList.get(j + 1)));
//                    }
//                    potentialSplits.put("cp", potentialCpList);
//                    System.out.println("potentialCpList = " + potentialCpList);
//                    break;
//
//                case 3: // first column - trestbps
//                    //get all list of cp
//                    List<Double> uniqueTrestbpsList = new ArrayList<>();
//                    data.stream().map(sc -> sc.getTrestbps()).distinct().sorted().forEach(uniqueTrestbpsList::add);
//                    List<Double> potentialTrestbpsList = new ArrayList<>();
//                    for (int j = 0; j < uniqueTrestbpsList.size() - 1; j++) {
//
//                        double splitValue = getSplitValue(uniqueTrestbpsList.get(j), uniqueTrestbpsList.get(j + 1));
//                        DataSet dataAbove = new DataSet();
//                        DataSet dataBelow = new DataSet();
//                        dataAbove.setHeartDataSet(data.stream().filter(d -> d.getTrestbps() > splitValue).collect(Collectors.toList()));
//                        dataBelow.setHeartDataSet(data.stream().filter(d -> d.getTrestbps() <= splitValue).collect(Collectors.toList()));
//
//                        DataSet[] dataSetArray = {dataBelow, dataAbove};
//                        double currentEntropy = culculateOverallEntropy(dataSetArray);
//                        if (currentEntropy < minEntropy)
//                            minEntropy = currentEntropy;
//                        Map newMap = new HashMap();
//                        newMap.put("trestbps", minEntropy);
//                        splitColumnAndValue.lazySet(newMap);
//                        potentialTrestbpsList.add(getSplitValue(uniqueTrestbpsList.get(j), uniqueTrestbpsList.get(j + 1)));
//                    }
//                    potentialSplits.put("trestbps", potentialTrestbpsList);
//                    System.out.println("potentialTrestbpsList = " + potentialTrestbpsList);
//                    break;
//
//                case 4: // first column - chol
//                    //get all list of chol
//                    List<Double> uniqueCholList = new ArrayList<>();
//                    data.stream().map(sc -> sc.getChol()).distinct().sorted().forEach(uniqueCholList::add);
//                    List<Double> potentialCholList = new ArrayList<>();
//                    for (int j = 0; j < uniqueCholList.size() - 1; j++) {
//                        double splitValue = getSplitValue(uniqueCholList.get(j), uniqueCholList.get(j + 1));
//                        DataSet dataAbove = new DataSet();
//                        DataSet dataBelow = new DataSet();
//                        dataAbove.setHeartDataSet(data.stream().filter(d -> d.getChol() > splitValue).collect(Collectors.toList()));
//                        dataBelow.setHeartDataSet(data.stream().filter(d -> d.getChol() <= splitValue).collect(Collectors.toList()));
//
//                        DataSet[] dataSetArray = {dataBelow, dataAbove};
//                        double currentEntropy = culculateOverallEntropy(dataSetArray);
//                        if (currentEntropy < minEntropy)
//                            minEntropy = currentEntropy;
//
//                        Map newMap = new HashMap();
//                        newMap.put("chol", minEntropy);
//                        splitColumnAndValue.lazySet(newMap);
//                        potentialCholList.add(getSplitValue(uniqueCholList.get(j), uniqueCholList.get(j + 1)));
//                    }
//                    potentialSplits.put("chol", potentialCholList);
//                    break;
//
//                case 5: // first column - fbs
//                    //get all list of fbs
//                    List<Double> uniqueFbsList = new ArrayList<>();
//                    data.stream().map(sc -> sc.getFbs()).distinct().sorted().forEach(uniqueFbsList::add);
//                    List<Double> potentialFbsList = new ArrayList<>();
//                    for (int j = 0; j < uniqueFbsList.size() - 1; j++) {
//                        double splitValue = getSplitValue(uniqueFbsList.get(j), uniqueFbsList.get(j + 1));
//                        DataSet dataAbove = new DataSet();
//                        DataSet dataBelow = new DataSet();
//                        dataAbove.setHeartDataSet(data.stream().filter(d -> d.getFbs() > splitValue).collect(Collectors.toList()));
//                        dataBelow.setHeartDataSet(data.stream().filter(d -> d.getFbs() <= splitValue).collect(Collectors.toList()));
//
//                        DataSet[] dataSetArray = {dataBelow, dataAbove};
//                        double currentEntropy = culculateOverallEntropy(dataSetArray);
//                        if (currentEntropy < minEntropy)
//                            minEntropy = currentEntropy;
//
//                        Map newMap = new HashMap();
//                        newMap.put("fbs", minEntropy);
//                        splitColumnAndValue.lazySet(newMap);
//                        potentialFbsList.add(getSplitValue(uniqueFbsList.get(j), uniqueFbsList.get(j + 1)));
//                    }
//                    potentialSplits.put("fbs", potentialFbsList);
//                    break;
//
//                case 6: // first column - restecg
//                    //get all list of restecg
//                    List<Double> uniqueRestecgList = new ArrayList<>();
//                    data.stream().map(sc -> sc.getRestecg()).distinct().sorted().forEach(uniqueRestecgList::add);
//                    List<Double> potentialRestecgList = new ArrayList<>();
//                    for (int j = 0; j < uniqueRestecgList.size() - 1; j++) {
//                        double splitValue = getSplitValue(uniqueRestecgList.get(j), uniqueRestecgList.get(j + 1));
//                        DataSet dataAbove = new DataSet();
//                        DataSet dataBelow = new DataSet();
//                        dataAbove.setHeartDataSet(data.stream().filter(d -> d.getRestecg() > splitValue).collect(Collectors.toList()));
//                        dataBelow.setHeartDataSet(data.stream().filter(d -> d.getRestecg() <= splitValue).collect(Collectors.toList()));
//
//                        DataSet[] dataSetArray = {dataBelow, dataAbove};
//                        double currentEntropy = culculateOverallEntropy(dataSetArray);
//                        if (currentEntropy < minEntropy)
//                            minEntropy = currentEntropy;
//
//                        Map newMap = new HashMap();
//                        newMap.put("restecg", minEntropy);
//                        splitColumnAndValue.lazySet(newMap);
//                        potentialRestecgList.add(getSplitValue(uniqueRestecgList.get(j), uniqueRestecgList.get(j + 1)));
//                    }
//                    potentialSplits.put("restecg", potentialRestecgList);
//                    break;
//
//                case 7: // first column - thalach
//                    //get all list of thalach
//                    List<Double> uniqueThalachList = new ArrayList<>();
//                    data.stream().map(sc -> sc.getThalach()).distinct().sorted().forEach(uniqueThalachList::add);
//                    List<Double> potentialThalachList = new ArrayList<>();
//                    for (int j = 0; j < uniqueThalachList.size() - 1; j++) {
//                        double splitValue = getSplitValue(uniqueThalachList.get(j), uniqueThalachList.get(j + 1));
//                        DataSet dataAbove = new DataSet();
//                        DataSet dataBelow = new DataSet();
//                        dataAbove.setHeartDataSet(data.stream().filter(d -> d.getThalach() > splitValue).collect(Collectors.toList()));
//                        dataBelow.setHeartDataSet(data.stream().filter(d -> d.getThalach() <= splitValue).collect(Collectors.toList()));
//
//                        DataSet[] dataSetArray = {dataBelow, dataAbove};
//                        double currentEntropy = culculateOverallEntropy(dataSetArray);
//                        if (currentEntropy < minEntropy)
//                            minEntropy = currentEntropy;
//
//                        Map newMap = new HashMap();
//                        newMap.put("thalach", minEntropy);
//                        splitColumnAndValue.lazySet(newMap);
//                        potentialThalachList.add(getSplitValue(uniqueThalachList.get(j), uniqueThalachList.get(j + 1)));
//                    }
//                    potentialSplits.put("thalach", potentialThalachList);
//                    break;
//
//                case 8: // first column - exang
//                    //get all list of exang
//                    List<Double> uniqueExangList = new ArrayList<>();
//                    data.stream().map(sc -> sc.getExang()).distinct().sorted().forEach(uniqueExangList::add);
//                    List<Double> potentialExangList = new ArrayList<>();
//                    for (int j = 0; j < uniqueExangList.size() - 1; j++) {
//                        double splitValue = getSplitValue(uniqueExangList.get(j), uniqueExangList.get(j + 1));
//                        DataSet dataAbove = new DataSet();
//                        DataSet dataBelow = new DataSet();
//                        dataAbove.setHeartDataSet(data.stream().filter(d -> d.getExang() > splitValue).collect(Collectors.toList()));
//                        dataBelow.setHeartDataSet(data.stream().filter(d -> d.getExang() <= splitValue).collect(Collectors.toList()));
//
//                        DataSet[] dataSetArray = {dataBelow, dataAbove};
//                        double currentEntropy = culculateOverallEntropy(dataSetArray);
//                        if (currentEntropy < minEntropy)
//                            minEntropy = currentEntropy;
//
//                        Map newMap = new HashMap();
//                        newMap.put("exang", minEntropy);
//                        splitColumnAndValue.lazySet(newMap);
//                        potentialExangList.add(getSplitValue(uniqueExangList.get(j), uniqueExangList.get(j + 1)));
//                    }
//                    potentialSplits.put("exang", potentialExangList);
//                    break;
//
//                case 9: // first column - oldpeak
//                    //get all list of oldpeak
//                    List<Double> uniqueOldPeakList = new ArrayList<>();
//                    data.stream().map(sc -> sc.getOldpeak()).distinct().sorted().forEach(uniqueOldPeakList::add);
//                    List<Double> potentialOldPeakList = new ArrayList<>();
//                    for (int j = 0; j < uniqueOldPeakList.size() - 1; j++) {
//                        double splitValue = getSplitValue(uniqueOldPeakList.get(j), uniqueOldPeakList.get(j + 1));
//                        DataSet dataAbove = new DataSet();
//                        DataSet dataBelow = new DataSet();
//                        dataAbove.setHeartDataSet(data.stream().filter(d -> d.getOldpeak() > splitValue).collect(Collectors.toList()));
//                        dataBelow.setHeartDataSet(data.stream().filter(d -> d.getOldpeak() <= splitValue).collect(Collectors.toList()));
//
//                        DataSet[] dataSetArray = {dataBelow, dataAbove};
//                        double currentEntropy = culculateOverallEntropy(dataSetArray);
//                        if (currentEntropy < minEntropy)
//                            minEntropy = currentEntropy;
//
//                        Map newMap = new HashMap();
//                        newMap.put("oldpeak", minEntropy);
//                        splitColumnAndValue.lazySet(newMap);
//                        potentialOldPeakList.add(getSplitValue(uniqueOldPeakList.get(j), uniqueOldPeakList.get(j + 1)));
//                    }
//                    potentialSplits.put("oldpeak", potentialOldPeakList);
//                    break;
//
//                case 10: // first column - slope
//                    //get all list of slope
//                    List<Double> uniqueSlopePeakList = new ArrayList<>();
//                    data.stream().map(sc -> sc.getSlope()).distinct().sorted().forEach(uniqueSlopePeakList::add);
//                    List<Double> potentialSlopePeakList = new ArrayList<>();
//                    for (int j = 0; j < uniqueSlopePeakList.size() - 1; j++) {
//                        double splitValue = getSplitValue(uniqueSlopePeakList.get(j), uniqueSlopePeakList.get(j + 1));
//                        DataSet dataAbove = new DataSet();
//                        DataSet dataBelow = new DataSet();
//                        dataAbove.setHeartDataSet(data.stream().filter(d -> d.getSlope() > splitValue).collect(Collectors.toList()));
//                        dataBelow.setHeartDataSet(data.stream().filter(d -> d.getSlope() <= splitValue).collect(Collectors.toList()));
//
//                        DataSet[] dataSetArray = {dataBelow, dataAbove};
//                        double currentEntropy = culculateOverallEntropy(dataSetArray);
//                        if (currentEntropy < minEntropy)
//                            minEntropy = currentEntropy;
//
//                        Map newMap = new HashMap();
//                        newMap.put("slope", minEntropy);
//                        splitColumnAndValue.lazySet(newMap);
//                        potentialSlopePeakList.add(getSplitValue(uniqueSlopePeakList.get(j), uniqueSlopePeakList.get(j + 1)));
//                    }
//                    potentialSplits.put("slope", potentialSlopePeakList);
//                    break;
//
//                case 11: // first column - ca
//                    //get all list of ca
//                    List<Double> uniqueCaList = new ArrayList<>();
//                    data.stream().map(sc -> sc.getCa()).distinct().sorted().forEach(uniqueCaList::add);
//                    List<Double> potentialCaList = new ArrayList<>();
//                    for (int j = 0; j < uniqueCaList.size() - 1; j++) {
//                        double splitValue = getSplitValue(uniqueCaList.get(j), uniqueCaList.get(j + 1));
//                        DataSet dataAbove = new DataSet();
//                        DataSet dataBelow = new DataSet();
//                        dataAbove.setHeartDataSet(data.stream().filter(d -> d.getCa() > splitValue).collect(Collectors.toList()));
//                        dataBelow.setHeartDataSet(data.stream().filter(d -> d.getCa() <= splitValue).collect(Collectors.toList()));
//
//                        DataSet[] dataSetArray = {dataBelow, dataAbove};
//                        double currentEntropy = culculateOverallEntropy(dataSetArray);
//                        if (currentEntropy < minEntropy)
//                            minEntropy = currentEntropy;
//
//                        Map newMap = new HashMap();
//                        newMap.put("ca", minEntropy);
//                        splitColumnAndValue.lazySet(newMap);
//                        potentialCaList.add(getSplitValue(uniqueCaList.get(j), uniqueCaList.get(j + 1)));
//                    }
//                    potentialSplits.put("ca", potentialCaList);
//                    break;
//
//
//                case 12: // first column - thal
//                    //get all list of thal
//                    List<Double> uniqueThalList = new ArrayList<>();
//                    data.stream().map(sc -> sc.getThal()).distinct().sorted().forEach(uniqueThalList::add);
//                    List<Double> potentialThalList = new ArrayList<>();
//                    for (int j = 0; j < uniqueThalList.size() - 1; j++) {
//                        double splitValue = getSplitValue(uniqueThalList.get(j), uniqueThalList.get(j + 1));
//                        DataSet dataAbove = new DataSet();
//                        DataSet dataBelow = new DataSet();
//                        dataAbove.setHeartDataSet(data.stream().filter(d -> d.getThal() > splitValue).collect(Collectors.toList()));
//                        dataBelow.setHeartDataSet(data.stream().filter(d -> d.getThal() <= splitValue).collect(Collectors.toList()));
//
//                        DataSet[] dataSetArray = {dataBelow, dataAbove};
//                        double currentEntropy = culculateOverallEntropy(dataSetArray);
//                        if (currentEntropy < minEntropy)
//                            minEntropy = currentEntropy;
//
//                        Map newMap = new HashMap();
//                        newMap.put("thal", minEntropy);
//                        splitColumnAndValue.lazySet(newMap);
//                        potentialThalList.add(getSplitValue(uniqueThalList.get(j), uniqueThalList.get(j + 1)));
//                    }
//                    potentialSplits.put("thal", potentialThalList);
//                    break;
//            }
//        }
//
//        // return splitColumnAndValue.get();
//    }
}
