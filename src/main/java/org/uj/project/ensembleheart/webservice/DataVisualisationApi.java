package org.uj.project.ensembleheart.webservice;

import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

/**
 * @author fchidzikwe
 */

@Controller
public class DataVisualisationApi {


    private String baseUrl = "http://localhost:8080/";




    @GetMapping("/data-visualisation")
    public String viewDataset(){
        return "data";
    }

    @GetMapping(value = "/missing-values-graph" , produces = MediaType.IMAGE_PNG_VALUE)
    public @ResponseBody byte[] missingValuesGrapgh(){
        String url =baseUrl + "/missing-values-graph";
        return getGraph(url);
    }

    @GetMapping(value = "/replaced-values-graph", produces = MediaType.IMAGE_PNG_VALUE)
    public @ResponseBody byte[] replacedValuesGraph(){
        String url =baseUrl + "/replaced-values-graph";
        return getGraph(url);
    }

    @GetMapping("/dataset-info")
    public String dataInfo(Model model){

        String url = baseUrl + "/feature-important";

        double[][] importanceArray = getFeatureImportance(url);


        model.addAttribute("age", importanceArray[0][0]);
        model.addAttribute("sex", importanceArray[1][0]);
        model.addAttribute("cp", importanceArray[2][0]);
        model.addAttribute("trestbps", importanceArray[3][0]);
        model.addAttribute("fbs", importanceArray[4][0]);
        model.addAttribute("chol", importanceArray[5][0]);
        model.addAttribute("restecg", importanceArray[6][0]);
        model.addAttribute("thalach", importanceArray[7][0]);
        model.addAttribute("exang", importanceArray[8][0]);
        model.addAttribute("oldpeak", importanceArray[9][0]);
        model.addAttribute("slope", importanceArray[10][0]);
        model.addAttribute("ca", importanceArray[11][0]);
        model.addAttribute("thal", importanceArray[12][0]);
        model.addAttribute("target", importanceArray[13][0]);


        return "data-info";
    }


    public byte[] getGraph(String url){

        RestTemplate template = new RestTemplate();

        List<MediaType> acceptableMediaTypes = new ArrayList<MediaType>();
        acceptableMediaTypes.add(MediaType.IMAGE_PNG);
        // Prepare header
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(acceptableMediaTypes);
        HttpEntity<String> entity = new HttpEntity<String>(headers);
        ResponseEntity<byte[]> responseEntity = template.exchange(url, HttpMethod.POST, entity, byte[].class);

        if(responseEntity.getStatusCode()== HttpStatus.OK){
                return responseEntity.getBody();
        }

        return null;
    }



    public double[][] getFeatureImportance(String url){

        RestTemplate template = new RestTemplate();

        List<MediaType> acceptableMediaTypes = new ArrayList<MediaType>();
        acceptableMediaTypes.add(MediaType.APPLICATION_JSON);
        // Prepare header
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(acceptableMediaTypes);
        HttpEntity<String> entity = new HttpEntity<String>(headers);
        ResponseEntity<double[][]> responseEntity = template.exchange(url, HttpMethod.POST, entity, double[][].class);

        if(responseEntity.getStatusCode()== HttpStatus.OK){
            return responseEntity.getBody();
        }

        return null;
    }

}
