package org.uj.project.ensembleheart.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.supercsv.cellprocessor.ParseDouble;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;
import org.uj.project.ensembleheart.constants.Data;
import org.uj.project.ensembleheart.decisiontree.DecisionTree;
import org.uj.project.ensembleheart.decisiontree.Node;
import org.uj.project.ensembleheart.model.*;
import org.uj.project.ensembleheart.neuralnet.NeuralNet;
import org.uj.project.ensembleheart.service.PredictionService;
import org.uj.project.ensembleheart.service.RoleService;
import org.uj.project.ensembleheart.service.UserService;
import org.uj.project.ensembleheart.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.*;

/**
 * @author fchidzikwe
 */

@Controller
public class RegistrationController {

    private PredictionService predictionService;
    private UserService userService;
    private RoleService roleService;
    private HandleData handleData;
    private CsvReader csvReader;
    private DataNormalisation dataNormalisation;
    private CSVUtils csvUtils;
    private CsvOps csvOps;
    private static String TESTINGDATASETNAME = "testing_set";
    private static String TRAININGDATASETNAME = "training_set";
    private static String DATASETNAME = "full_dataset";
    private static String NORMALISEDDATASETNAME = "normalised_full_dataset";

    @Autowired
    public RegistrationController(CsvOps csvOps , DataNormalisation dataNormalisation,CsvReader csvReader,HandleData handleData, UserService userService, RoleService roleService, PredictionService predictionService) {
        this.handleData = handleData;
        this.userService = userService;
        this.roleService = roleService;
        this.csvReader =csvReader;
        this.dataNormalisation=dataNormalisation;
        this.predictionService =predictionService;
        this.csvOps = csvOps;
    }

    @GetMapping("/login")
    public String getLoginPage(){
        return "login";
    }

    @GetMapping(value = "/home")
    public String getHomePage(Model model){
        User user = userService.getLoggedInUser();
        List<Prediction> predictionList = predictionService.findByUser(user);
        String  user_age =   calculateUserAge(user) + " yrs";
        user.setAge(user_age);
        model.addAttribute("user", user);
        model.addAttribute("predictionList", predictionList);
        return "home";
    }

    @GetMapping("/")
    public String getSearchPage(Model model){
        Map<String, Double> lineChartPlot = new HashMap<>();
        model.addAttribute("lineChartPlot", lineChartPlot);
        return "index";
    }

    @GetMapping("/plot")
    public String plot(Model model){
        return "chart";

    }


    @GetMapping("/normalise-dataset")
    public String normaliseData(){
        DataSet dataSet = new DataSet();
        try {
            dataSet=  csvReader.readCsv(Data.getDataFileLocation());
        } catch (IOException e) {
            e.printStackTrace();
        }
        dataSet.setDatasetName(DATASETNAME);

        DataSet normalisedDataset = null;
        try {
            normalisedDataset= dataNormalisation.normaliseData(dataSet);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        //write normalised data to csv
        normalisedDataset.setDatasetName(Data.getNormalisedDataFileLocation());
        writeCSVFile(normalisedDataset);
        return "home";
    }


    @GetMapping("/compare-algo")
    public  String compareAlgorithms(Model model) throws IOException, IllegalAccessException {

        List<Experiment> experiements = new ArrayList<Experiment>();

        // make experiemtn for nn

        DataSet nnTrainingDataSet = csvOps.readCsvAndParseToObject(Data.getNormalisedTrainingDataFileLocation());
        DataSet nnTestingDataSet = csvOps.readCsvAndParseToObject(Data.getNormalisedTestingDataFileLocation());



        // DataSet trainingDataSet = csvReader.readCsv(Data.getTrainingDataFileLocation());
        NeuralNet neuralNet = new NeuralNet(13,20,1);
        neuralNet.initialiseWeights();
        Double [] epochErrors =null;
        // train nn
        epochErrors = neuralNet.startTrainingToError(nnTrainingDataSet, 0.23, 800);
        // save nn
        neuralNet.saveTestedNeuralNetwork(neuralNet);
        double neuralNetworkAccuracy=  testNeuralNetwork(neuralNet, model, nnTestingDataSet);

        Experiment nnExperiment = new Experiment();
        nnExperiment.setAccuracy(neuralNetworkAccuracy);
        nnExperiment.setAlgorithmName("NN");
        nnExperiment.setPercentageMissing("0");


        experiements.add(nnExperiment);

        // make experiemtn for dt


        //for decision tree
        DataSet dtTrainingDataSet = csvOps.readCsvAndParseToObject(Data.getDataFileLocation());
        DecisionTree decisionTree = new DecisionTree();
        Node rootNode = new Node(dtTrainingDataSet.getHeartDataSet());
        rootNode.setParent(null);
        // use depth first search
        Stack<Node> tree = new Stack<Node>();
        Stack<Node> printTree = new Stack<Node>();
        printTree.push(rootNode);
        // before passing the node in the training tree check if it is pure
        if (!decisionTree.isPure(rootNode, 0.7)) {
            tree.push(rootNode);
            decisionTree.startTraining( 0.75, tree, printTree);
        }
        // test dt with 5 rows
        double testSize =dtTrainingDataSet.getHeartDataSet().size();



        int dTCorrectCounts = 0;

        for(int i =0; i< testSize; i++){
            double input [] = dtTrainingDataSet.getHeartDataSet().get(i).getInputArray();
            double desired [] = dtTrainingDataSet.getHeartDataSet().get(i).getOutputArray();
            double output = decisionTree.testDecistionTree(input, printTree);
            if(output == desired[0]){
                dTCorrectCounts ++;
            }
        }


        double decisionTreeAcuuracy = dTCorrectCounts / testSize;


        Experiment dtExperiment = new Experiment();
        dtExperiment.setAccuracy(decisionTreeAcuuracy);
        dtExperiment.setAlgorithmName("DT");
        dtExperiment.setPercentageMissing("0");

        experiements.add(dtExperiment);


        model.addAttribute("experiments", experiements);

        System.out.println(" decision tree accuuracy : " + decisionTreeAcuuracy);
        System.out.println("neural network accuracy : " + neuralNetworkAccuracy);

        return "experiment";
    }

    @GetMapping("/nn-train-test")
    public String readCsv(Model model) throws IOException, IllegalAccessException {

        DataSet nnTrainingDataSet = csvOps.readCsvAndParseToObject(Data.getNormalisedTrainingDataFileLocation());
        DataSet nnTestingDataSet = csvOps.readCsvAndParseToObject(Data.getNormalisedTestingDataFileLocation());



       // DataSet trainingDataSet = csvReader.readCsv(Data.getTrainingDataFileLocation());
        NeuralNet neuralNet = new NeuralNet(13,20,1);
        neuralNet.initialiseWeights();
        Double [] epochErrors =null;
        // train nn
        epochErrors = neuralNet.startTrainingToError(nnTrainingDataSet, 0.23, 800);
        // save nn
        neuralNet.saveTestedNeuralNetwork(neuralNet);
       double accuracy=  testNeuralNetwork(neuralNet, model, nnTestingDataSet);

        List<Integer>  iterations = new ArrayList<>();
        List<Double> errors = Arrays.asList(epochErrors);
        model.addAttribute("iterations", iterations);
        model.addAttribute("errors", errors);
        model.addAttribute("accuracy", accuracy);
        return "chart";
    }

    private double testNeuralNetwork(NeuralNet neuralNet, Model model , DataSet testingDataSet) throws IOException, IllegalAccessException {

        System.out.println(" >>>>>>>>>>>>>>>>>>>>>>testing neural Network with testing size "+ testingDataSet.getHeartDataSet().size());

        double correctClassficationCount =0;
        for (int i = 0; i <testingDataSet.getHeartDataSet().size(); i++) {

            double input[] = new double[testingDataSet.getHeartDataSet().get(i).getInputArray().length];
            double desired[] = new double[testingDataSet.getHeartDataSet().get(i).getOutputArray().length];
            System.arraycopy(testingDataSet.getHeartDataSet().get(i).getInputArray(), 0, input, 0, testingDataSet.getHeartDataSet().get(i).getInputArray().length);
            System.arraycopy(testingDataSet.getHeartDataSet().get(i).getOutputArray(), 0, desired, 0, testingDataSet.getHeartDataSet().get(i).getOutputArray().length);
            double[] output = neuralNet.forwardPass(input);
            //compare desired and the output
            for(int a=0;a< desired.length;a++){
              double diff = Math.abs(desired[a] - output[a]);
                System.out.println("+++++++++++++ desired :" + desired[a] + " output: " + output[a]);
                System.out.println("++++++++++++++++ difference " + diff);
              if(diff <  0.6){
                  correctClassficationCount++;
              }
            }
        }
        System.out.println("+++++++++++++++++++++++++ correct classification count "+ correctClassficationCount);
        double accuracy = correctClassficationCount / testingDataSet.getHeartDataSet().size() * 0.8;
        return  accuracy;
    }

    private void  createTrainingAndTestingDataSet(DataSet normalisedDataset) {
        DataSet trainingSet =new DataSet();
        DataSet testingSet =new DataSet();
        testingSet.setDatasetName(TESTINGDATASETNAME);
        trainingSet.setDatasetName(TRAININGDATASETNAME);
        List<HeartData> trainng = new ArrayList<>();
        List<HeartData> testing = new ArrayList<>();

        ///splitting criteria training = 70%,  testing = 30%
        int sizeOfDataset = normalisedDataset.getHeartDataSet().size();
        int trainingSetSize = (int)Math.ceil(sizeOfDataset * 0.7);
        for(int i=0; i< sizeOfDataset;i++){
            if(i< trainingSetSize){
                trainng.add(normalisedDataset.getHeartDataSet().get(i));
            }else{
                testing.add(normalisedDataset.getHeartDataSet().get(i));
            }
        }
        trainingSet.setHeartDataSet(trainng);
        trainingSet.setDatasetName(Data.getTrainingDataFileLocation());
        testingSet.setHeartDataSet(testing);
        testingSet.setDatasetName(Data.getTestingDataFileLocation());
        //write to csv
        writeCSVFile(trainingSet);
        writeCSVFile(testingSet);


    }

    static void writeCSVFile(DataSet dataSet) {
        ICsvBeanWriter beanWriter = null;
        CellProcessor[] processors = new CellProcessor[] {
                new ParseDouble() ,new ParseDouble(), new ParseDouble(),
                new ParseDouble() ,new ParseDouble() ,new ParseDouble(),
                new ParseDouble() ,new ParseDouble() ,new ParseDouble(),
                new ParseDouble(), new ParseDouble(), new ParseDouble(),
                new ParseDouble() ,new ParseDouble(),

        };
        try {
            beanWriter = new CsvBeanWriter(new FileWriter(dataSet.getDatasetName()),
                    CsvPreference.STANDARD_PREFERENCE);
            String[] header = { "age","sex","cp","trestbps","chol","fbs","restecg","thalach","exang","oldpeak","slope","ca","thal","target"};
            for (HeartData heartData : dataSet.getHeartDataSet()) {
                beanWriter.write(heartData,header, processors);
            }
        } catch (IOException ex) {
            System.err.println("Error writing the CSV file: " + ex);
        } finally {
            if (beanWriter != null) {
                try {
                    beanWriter.close();
                } catch (IOException ex) {
                    System.err.println("Error closing the writer: " + ex);
                }
            }
        }
    }

    @GetMapping("/register")
    public String getRegistrationPage(Model model){
        List<Role> roleList = new ArrayList<>();;
        model.addAttribute("user", new User());
        return "register";
    }
    // Process form input data
    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String createUser(Model model, RedirectAttributes redirectAttributes, @Valid User user, BindingResult bindingResul, HttpServletRequest request, @RequestParam("dob") String dob) {
        System.out.println("==================user passed "+ user.toString());
        User userExists = userService.findByEmail(user.getEmail());
        if (userExists != null) {
            redirectAttributes.addFlashAttribute("msg", "USER ALREADY EXISTS");
            redirectAttributes.addFlashAttribute("css", "text-danger");
        }
        Role patientRole = roleService.findByRoleName("PATIENT");
        Set<Role>  roles = new HashSet<>();

        roles.add(patientRole);
        Date date = DateConveter.stringToDate(dob);
        user.setDob(date);
        user.setRoles(roles);
        userService.saveUser(user);
        redirectAttributes.addFlashAttribute("msg", "USER SUCCESSFULLY REGISTERED");
        redirectAttributes.addFlashAttribute("css", "text-success");
        return  "redirect:/register";

    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.PUT)
    public String updateUser(@PathVariable("id") long id, Model model) {
        User dbUser = userService.findById(id);
        if (dbUser == null) {
            model.addAttribute("msg", "user not found!");
            model.addAttribute("css", "text-danger");
        }
        model.addAttribute("msg", "user updated!");
        model.addAttribute("css", "text-success");
        userService.saveUser(dbUser);
        return "register";
    }


    @RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE)
    public String deleteUser(@PathVariable("id") long id, Model model) {
        User user = userService.findById(id);
        if (user == null) {
            model.addAttribute("msg", "user not found!");
            model.addAttribute("css", "text-danger");
        }
        userService.deleteUser(user.getId());
        model.addAttribute("msg", "user deleted!");
        model.addAttribute("css", "text-success");
        return "redirect:/list-users";
    }

    @RequestMapping(value="/list-users", method = RequestMethod.GET)
    public String getUsers(Model model) {
        List<User> user = userService.findAll();
        model.addAttribute("users", user);
        return "users/list";
    }


    private int calculateUserAge(User user) {
        LocalDate today = LocalDate.now();
        LocalDate dob = user.getDob().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        return Period.between(dob, today).getYears();
    }

}
