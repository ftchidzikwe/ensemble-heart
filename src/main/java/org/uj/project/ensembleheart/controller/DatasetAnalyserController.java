package org.uj.project.ensembleheart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.uj.project.ensembleheart.constants.Data;
import org.uj.project.ensembleheart.model.DataSet;
import org.uj.project.ensembleheart.util.CsvReader;
import org.uj.project.ensembleheart.util.DataNormalisation;

import java.io.IOException;

import static org.uj.project.ensembleheart.controller.RegistrationController.writeCSVFile;


/**
 * @author fchidzikwe
 */

@Controller
public class DatasetAnalyserController {


    private static String TESTINGDATASETNAME = "testing_set";
    private static String TRAININGDATASETNAME = "training_set";
    private static String DATASETNAME = "full_dataset";
    private static String NORMALISEDDATASETNAME = "normalised_full_dataset";

    private CsvReader csvReader;
    private DataNormalisation dataNormalisation;

    @Autowired
    public DatasetAnalyserController(CsvReader csvReader,  DataNormalisation dataNormalisation) {
        this.csvReader = csvReader;
        this.dataNormalisation =dataNormalisation;
    }




    @GetMapping("/analyse-dataset")
    public String analyse(Model model) throws IOException, IllegalAccessException {

        DataSet dataSet = new DataSet();


        try {
            dataSet=  csvReader.readCsv(Data.getDataFileLocation());
        } catch (IOException e) {
            e.printStackTrace();
        }
        dataSet.setDatasetName(DATASETNAME);



        DataSet normalisedDataset = null;
        try {
            normalisedDataset= dataNormalisation.normaliseData(dataSet);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        //write normalised data to csv
        normalisedDataset.setDatasetName(Data.getNormalisedDataFileLocation());

        writeCSVFile(normalisedDataset);


        //createTrainingAndTestingDataSet(normalisedDataset);

        DataSet trainingDataSet = csvReader.readCsv(Data.getTrainingDataFileLocation());





        return "dataset";
    }
}
