package org.uj.project.ensembleheart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Paths;

/**
 * @author fchidzikwe
 */

@Controller
public class CsvUploadController {

    @Autowired
    private Environment environment;

    @GetMapping("/upload-csv")
    public String getUploadCsvForm(Model model) {

        //if file exists preview

        String uploadFilePath = Paths.get("." + File.separator + environment.getProperty("spring.config.location")).toString();

        File uploadDirectory = new File(uploadFilePath);

        if (uploadDirectory.isDirectory()) {


            System.out.println("************************ this is a directory");

            // directory contains training data
            if (uploadDirectory.list().length > 0) {

                model.addAttribute("uploadState", 1);

                System.out.println("*************** there is a file");

            } else {
                model.addAttribute("uploadState", 0);

                System.out.println("*************** there is no file present");

            }


            //else upload

        } else {
            System.out.println("************************ this is  not a directory");
        }
        return "csv/csv-upload";
    }


    @PostMapping(value = "/upload-csv", headers = "content-type=multipart/form-data")
    public String uploadCsv(@RequestParam("uploadFile") MultipartFile uploadFile, RedirectAttributes model) {

        try {
            String filename = uploadFile.getOriginalFilename();
            String directory = environment.getProperty("spring.config.location");
            String uploadFilePath = Paths.get("." + File.separator + directory, filename).toString();
            final BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(uploadFilePath)));
            stream.write(uploadFile.getBytes());

            model.addAttribute("msg", " Successfully  Uploaded File :" + uploadFile.getOriginalFilename());
            model.addAttribute("css", "success");
            stream.close();

        } catch (Exception e) {
            model.addAttribute("msg", uploadFile.getOriginalFilename() + " upload  failed -->  Error :" + e.getMessage());
            model.addAttribute("css", "danger");

        }


        return "redirect:/csv-upload";

    }
}
