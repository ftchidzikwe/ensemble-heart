package org.uj.project.ensembleheart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.uj.project.ensembleheart.model.DataManipulation;
import org.uj.project.ensembleheart.model.Missingness;
import org.uj.project.ensembleheart.service.DataManipulationService;

import javax.validation.Valid;
import java.util.List;

/**
 * @author fchidzikwe
 */

@Controller
@RequestMapping("/data-manipulation")
public class DataManipulationController {

    private DataManipulationService dataManipulationService;


    @Autowired
    public DataManipulationController(DataManipulationService dataManipulationService) {
        this.dataManipulationService = dataManipulationService;
    }

    /**
     * This controller is responsible for getting the different manipluation that a user wants to carry out
     */


    @GetMapping(value = {"/add", "/add/{id}"})
    public String add(Model model, @PathVariable(value = "id", required = false) Long id) {

        DataManipulation dataManipulation = null;
        if (id != null) {
            dataManipulation = dataManipulationService.findById(id).orElse(null);

        }


        List<DataManipulation> dataManipulationList = dataManipulationService.findAll();

        dataManipulationList.stream().forEach(dm -> {
            Missingness.getMissingValuesAsList().remove(dm.getPercentageMissing());
        });

        model.addAttribute("missingValueList", Missingness.getMissingValuesAsList());

        if (dataManipulation != null) {
            model.addAttribute("dataManipulation", dataManipulation);
            model.addAttribute("title", "Update");
        } else {
            model.addAttribute("dataManipulation", new DataManipulation());
            model.addAttribute("title", "Add");
        }
        return "data/add";
    }

    @PostMapping("/add")
    public String add(@Valid DataManipulation dataManipulation, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            bindingResult.reject(bindingResult.getFieldError().getField());
            model.addAttribute("msg", bindingResult.getFieldError());
            return "data/list";
        } else {

            DataManipulation savedManipulation = dataManipulationService.save(dataManipulation);
            redirectAttributes.addFlashAttribute("msg", "Successfully added with id : " + dataManipulation.toString());
            redirectAttributes.addFlashAttribute("css", "text-success");

            return "redirect:/data-manipulation/list";
        }
    }

    @GetMapping("/list")
    public String list(Model model) {
        model.addAttribute("title", "List");
        model.addAttribute("list", dataManipulationService.findAll());
        return "data/list";
    }


    @GetMapping("/delete/{id}")
    public String list(RedirectAttributes redirectAttributes, @PathVariable(value = "id", required = false) Long id) {
        DataManipulation dataManipulation = dataManipulationService.findById(id).orElse(null);
        dataManipulationService.deleteDatamanipulation(dataManipulation);
        redirectAttributes.addFlashAttribute("msg", "Deleted");
        redirectAttributes.addFlashAttribute("css", "text-success");
        return "redirect:/data-manipulation/list";
    }


}
