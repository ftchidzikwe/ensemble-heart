package org.uj.project.ensembleheart.controller;


import org.apache.poi.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.uj.project.ensembleheart.exception.InvalidFileException;
import org.uj.project.ensembleheart.model.File;
import org.uj.project.ensembleheart.service.FileService;
import org.uj.project.ensembleheart.service.UserService;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

/**
 * @author fchidzikwe
 */
@Controller
@RequestMapping("/banner")
public class BannerController {
    private static final Logger logger = LoggerFactory.getLogger(BannerController.class);


    @Value("${upload.file.directory}")
    private String uploadDirectory;

    @Autowired
    private FileService fileService;

    @Autowired
    private UserService userService;



    @GetMapping("/upload-banner/{id}")
    public String uploadBanner(Model model, @PathVariable(value = "id") Long id){
        model.addAttribute("companyId", id);
        return "banner/upload";
    }

    @RequestMapping(value="/{id}/{fileName}", method = RequestMethod.GET, produces = MediaType.IMAGE_PNG_VALUE)
    public @ResponseBody
    byte[] profilePicture(@PathVariable("id") Long companyId, @PathVariable("fileName") String fileName ) throws IOException {
        String profilePicturePath = uploadDirectory+ java.io.File.separator + companyId+ java.io.File.separator + fileName;
        System.out.println("-------------------profile path in banner controller "+ profilePicturePath);
        if(!new java.io.File(profilePicturePath).exists()) {
            return null;
        }

        return  IOUtils.toByteArray(new FileInputStream(profilePicturePath));
    }


    @RequestMapping(value="/upload", method=RequestMethod.POST)
    String fileUploads(Model model, @RequestParam("file") MultipartFile [] files, @RequestParam("companyId") Long companyId) {

        String directory = uploadDirectory+"/"+ companyId;
        for(MultipartFile multipartFile : files){
            try {
                File uploadedFile = fileService.uploadFile(multipartFile, directory);
                fileService.save(uploadedFile);
            } catch (InvalidFileException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return "redirect:/";
    }

    @RequestMapping(value="/file", method=RequestMethod.GET)
    @ResponseBody
    ResponseEntity<InputStreamResource> uploadedFile() throws IOException {
       List<Path> pathList = new ArrayList<>();
        Path filePath = null;


        return ResponseEntity
                .ok()
                .contentLength(Files.size(filePath))
                .contentType(
                        MediaType.parseMediaType(
                                URLConnection.guessContentTypeFromName(filePath.toString())
                        )
                )
                .body(new InputStreamResource(
                        Files.newInputStream(filePath, StandardOpenOption.READ))
                );



    }



}
