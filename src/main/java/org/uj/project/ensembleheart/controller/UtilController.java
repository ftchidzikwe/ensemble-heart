package org.uj.project.ensembleheart.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author fchidzikwe
 */
@Controller
public class UtilController {

    @GetMapping("/access-denied")
    public String accessDenied() {
        return "error/access-denied";
    }


    @GetMapping("/error")
    public String errorPage() {
        return "error/error";
    }
}
