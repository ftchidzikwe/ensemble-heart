package org.uj.project.ensembleheart.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.uj.project.ensembleheart.model.Patient;
import org.uj.project.ensembleheart.model.Role;
import org.uj.project.ensembleheart.repository.UserRepository;
import org.uj.project.ensembleheart.service.PatientService;
import org.uj.project.ensembleheart.service.UserService;

import java.util.ArrayList;
import java.util.List;

/**
 * @author fchidzikwe
 */
@Controller
@RequestMapping("/search-utils")
public class PatientSearchController {

    private Logger logger = LoggerFactory.getLogger(PatientSearchController.class);


    @Autowired
    @Qualifier("userRepository")
    private UserRepository userRepository;

    @Autowired
    private PatientService patientService;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "")
    public String util() {
        return "search-utils";
    }

    @GetMapping("")
    public String callPojo(Model model) {
        model.addAttribute("patient", new Patient());
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Role role = userService.findByUsername(authentication.getName()).getRoles().stream().findAny().get();

        if(role.getName().equalsIgnoreCase("ADMIN")){
            model.addAttribute("role", "ADMIN");
        }else {
            model.addAttribute("role", "NURSE");
        }
        return "patient/search";
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public String callPojo(@ModelAttribute Patient patient, Model model) {


        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Role role = userService.findByUsername(authentication.getName()).getRoles().stream().findAny().get();

        if(role.getName().equalsIgnoreCase("ADMIN")){
            model.addAttribute("role", "ADMIN");
        }else {
            model.addAttribute("role", "NURSE");
        }
        model.addAttribute("patient", patient);
        model.addAttribute("search", patientService.findPatients(patient.getLastName()));
        return "patient/search";
    }

    @RequestMapping(value = "/getPatients/", method = RequestMethod.GET)
    @ResponseBody
    public List<Patient> getPatientList( @RequestParam String lastName) {
        List<Patient> patients = patientService.findPatients(lastName);

        List<Patient> arrayList = new ArrayList();
        arrayList.addAll(patients);
        patients.clear();
        for (Patient c : arrayList) {
            logger.debug("=====================looping for "+c);
            c.setId(c.getId());
            c.setFirstName(null);
            c.setLastName(null);
            patients.add(c);
        }
        return patients;

    }

}


