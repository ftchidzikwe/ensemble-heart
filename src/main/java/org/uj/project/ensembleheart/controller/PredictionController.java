package org.uj.project.ensembleheart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.uj.project.ensembleheart.model.Prediction;
import org.uj.project.ensembleheart.model.Symptoms;
import org.uj.project.ensembleheart.model.User;
import org.uj.project.ensembleheart.neuralnet.NeuralNet;
import org.uj.project.ensembleheart.service.PredictionService;
import org.uj.project.ensembleheart.service.SymptomsService;
import org.uj.project.ensembleheart.service.UserService;

import javax.validation.Valid;
import java.util.Date;

/**
 * @author fchidzikwe
 */

@Controller
public class PredictionController {


    private UserService userService;
    private PredictionService predictionService;
    private SymptomsService symptomsService;


    @Autowired
    public PredictionController(UserService userService, PredictionService predictionService, SymptomsService symptomsService) {
        this.userService = userService;
        this.predictionService = predictionService;
        this.symptomsService = symptomsService;
    }

    @GetMapping("/heart-disease")
    public String getPredictionPage(Model model){
        model.addAttribute("symptoms", new Symptoms());
        return  "prediction";
    }

    @PostMapping("/save-prediction")
    public  String makePrediciton(@Valid Symptoms symptoms, RedirectAttributes redirectAttributes){
        NeuralNet predicter = NeuralNet.retrieveTestedNN();
        User user = userService.getLoggedInUser();

        double [] symptomsArray = makeSymptomsArray(symptoms);

        double [] results = predicter.forwardPass(symptomsArray);

        Prediction prediction = new Prediction();


        prediction.setUser(user);
        prediction.setConfidence(results[0] * 100);
        prediction.setPredictionDate(new Date());

        if(results[0]> 0.5){
            prediction.setResult("Positive");
        }
        else {
            prediction.setResult("Negative");
        }

        if(results[0] >= 0 && results[0] <= 0.4){
            prediction.setColor("green_dot");
        }
        if(results[0] > 0.4 && results[0] < 0.5){
            prediction.setColor("yellow_dot");
        }
        if(results[0] >= 0.5){
            prediction.setColor("red_dot");
        }

        predictionService.save(prediction);

        return "redirect:/home";
    }

    double[]  makeSymptomsArray(Symptoms symptoms){

        double array [] =  new double[13];

         double maxAge = 77.0, minAge =29.0;
         double maxSex =1.0, minSex=0.0;
         double maxCp=3.0, minCp=0.0;
         double maxTrestbps=200.0, minTrestbps= 94.0;
         double maxChol=564.0, minChol=126.0;
         double maxFbs=1.0, minFbs=0.0;
         double maxRestecg=2.0, minRestecg=0.0;
         double maxThalach = 202.0, minThalach=71.0;
         double maxExang=1.0, minExang=0.0;
         double maxOldPeak=6.2, minOldPeak=0.0;
         double maxSlope=2.0, minSlope=0.0;
         double maxCa=4.0, minCa=0.0;
         double maxThal=3.0, minThal=0.0;


       double age = symptoms.getAge();
        double sex=  symptoms.getSex();
       double cp= symptoms.getCp();
        double trestbps=symptoms.getTrestbps();
        double chol = symptoms.getChol();
        double fbs = symptoms.getFbs();
       double restecg =symptoms.getRestecg();
       double thalach = symptoms.getThalach();
       double exang =symptoms.getExang();
        double oldpeak = symptoms.getOldpeak();
        double slope = symptoms.getSlope();
        double ca  = symptoms.getCa();
       double thal = symptoms.getThal();



        double nAge= (age - minAge)/(maxAge - minAge);
        double nSex= (sex - minSex)/(maxSex - minSex);
        double nCp=  (cp - minCp)/(maxCp - minCp);
        double nTrestbps= (trestbps - minTrestbps)/(maxTrestbps - minTrestbps);
        double nChol =(chol - minChol)/(maxChol - minChol);
        double nFbs =(fbs - minFbs)/(maxFbs - minFbs);
        double nRestecg =(restecg - minRestecg)/(maxRestecg - minRestecg);
        double nThalach = (thalach - minThalach)/(maxThalach - minThalach);
        double nExang =  (exang - minExang)/(maxExang - minExang);
        double nOldpeak = (oldpeak - minOldPeak)/(maxOldPeak - minOldPeak);
        double nSlope =(slope - minSlope)/(maxSlope - minSlope);
        double nCa =(ca - minCa)/(maxCa - minCa);
        double nThal = (thal - minThal)/(maxThal - minThal);



        array[0] = nAge;
        array[1] = nSex;
        array[2] = nCp;
        array[3] = nTrestbps;
        array[4] = nChol;
        array[5] = nFbs;
        array[6] = nRestecg;
        array[7] = nThalach;
        array[8] = nExang;
        array[9] = nOldpeak;
        array[10] = nSlope;
        array[11] = nCa;
        array[12] = nThal;


        return array;
    }
}
