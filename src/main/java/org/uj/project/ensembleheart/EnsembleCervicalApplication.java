package org.uj.project.ensembleheart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EnsembleCervicalApplication {

	public static void main(String[] args) {
		SpringApplication.run(EnsembleCervicalApplication.class, args);
	}
}
